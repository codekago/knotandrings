$(document).ready(function() {

	function loader() {
        $('body').oLoader({
            wholeWindow: true, //makes the loader fit the window size
            lockOverflow: true, //disable scrollbar on body

            backgroundColor: '#000',
            fadeInTime: 1000,
            fadeLevel: 0.4,
            image: 'img/loader.gif',
            //hideAfter: 1500
        });
    }//end of loader function

    $('#profile-review').load('include/profile_review.php');

    $('#followBtn').click(function() {
      var following = $('#followBtn').attr('rel');

      $('#follow-loader').css('display', 'block');
      $('#follow-alert').load('include/follow.php', {'type': 'follow', 'following': following});
});

    $('#unfollowBtn').click(function() {
      var following = $('#unfollowBtn').attr('rel');

      $('#follow-loader').css('display', 'block');
      $('#follow-alert').load('include/follow.php', {'type': 'unfollow', 'following': following});
});

    $('.msgBtn').click(function() {
      var user = $(this).attr('id');
      $('#message').focus();
      $('#msgUser').text(user);
});

    $('#msgForm').submit(function() {
var msg = $('#message').val();
var user = $('#msgUser').text();
loader();

$('#msgAlert').load('include/message.php', {'user': user, 'msg': msg});

return false;
    });

    //infinite scroll with masonry

  $(function(){
    
    var $container = $('.js-masonry');

    $container.imagesLoaded(function(){
      $container.masonry({
        itemSelector: '.grid-item',
        columnWidth: 200
      });
    });
    
    $container.infinitescroll({
      navSelector  : '#page-nav',    // selector for the paged navigation 
      nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
      itemSelector : '.grid-item',     // selector for all items you'll retrieve
      loading: {
          finishedMsg: 'No more post to load.',
          img: 'http://i.imgur.com/6RMhx.gif'
        }
      },
      // trigger Masonry as a callback
      function( newElements ) {
        // hide new items while they are loading
        var $newElems = $( newElements ).css({ opacity: 0 });
        // ensure that images load before adding to masonry layout
        $newElems.imagesLoaded(function(){
          // show elems now they're ready
          $newElems.animate({ opacity: 1 });
          $container.masonry( 'appended', $newElems, true ); 
        });
      }
    );
    
  });

   //alert('good');

//load the last message
    $('#messageLoad').load('include/message_load.php');
    $('#conversation_load').load('include/conversation_load.php');

$('#sendMessage').submit(function() {
var id = localStorage.getItem('conversation_id');
var msg = $('#userMessage').val();

$('#messagesLoader').css('display', 'block');
$('#conversationLoader').css('display', 'block');

$('#messageAlert').load('include/send_message.php', {'id': id, 'msg': msg});

return false;
});


$('.1').click(function() {
  $('.1').removeClass('fa-star-o');
  $('.2, .3, .4, .5').addClass('fa-star-o');
  $('.1').addClass('fa-star');
  $('#rate-status').text("poor");

  localStorage.setItem('rate-number', '1');
});

$('.2').click(function() {
  $('.1, .2').removeClass('fa-star-o');
  $('.1, .2').addClass('fa-star');
  $('.3, .4, .5').addClass('fa-star-o');
  $('#rate-status').text("okay");

  localStorage.setItem('rate-number', '2');
});

$('.3').click(function() {
  $('.1, .2, .3').removeClass('fa-star-o');
  $('.4, .5').addClass('fa-star-o');
  $('.1, .2, .3').addClass('fa-star');
  $('#rate-status').text("good");

  localStorage.setItem('rate-number', '3');
});

$('.4').click(function() {
  $('.1, .2, .3, .4').removeClass('fa-star-o');
  $('.5').addClass('fa-star-o');
  $('.1, .2, .3, .4').addClass('fa-star');
  $('#rate-status').text("awesome");

  localStorage.setItem('rate-number', '4');
});

$('.5').click(function() {
  $('.1, .2, .3, .4, .5').removeClass('fa-star-o');
  $('.1, .2, .3, .4, .5').addClass('fa-star');
  $('#rate-status').text("It's excellent");

  localStorage.setItem('rate-number', '5');
});

$('#rate-form').submit(function() {
	var title = $('#rate-title').val();
	var desc = $('#rate-description').val();
	var user = $('#rate-user').val();
	var id = $('#rate-id').val();
	var rate = localStorage.getItem('rate-number');


	if (rate != '') {
	loader();
		$('#rate-status').load('include/review.php', {'title': title, 'desc': desc, 'user': user, 'id': id, 'rate': rate});
	}else{
		alert('You have not rated this post yet.');
	}

return false;
});

});