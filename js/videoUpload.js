    $('document').ready(function(){
    ///////////////////upload function
    
    function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
    
    // Only process image files.
    if (f.type.match('image.*')) {
    continue;
    }
    
    var reader = new FileReader();
    
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
    return function(e) {
    // Render thumbnail.
    var span = document.getElementById('span');
    span.innerHTML = ['<video autoplay muted id="video" style="height: 100px; width: 100px;" src="', e.target.result,
		      '" title="', escape(theFile.name), '"/>'].join('');
    $('#video').css('display', 'block');
    };
    })(f);
    
    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
    }
    }
    document.getElementById('image').addEventListener('change', handleFileSelect, false);
    
    
    ////////////////////////////////end of upload function...............
      });