<?php
include('../../include/connect.php');
include('../../include/connect2.php');

if(isset($_REQUEST['id'])){
  //get the source
  $q = $conn->prepare("SELECT * FROM post WHERE id = :id");
  $q->bindParam(':id', $_REQUEST['id']);

  $q->execute();
  $row = $q->fetch();
  $source = "../../".$row['source'];

  //delete from folder
  $source_del = unlink($source);

  if($source_del){
    //delete from database
    $q = $conn->prepare("DELETE FROM post WHERE id = :id");
    $q->bindParam(':id', $_REQUEST['id']);
    $q->execute();

    //delete from database
    $q1 = $conn->prepare("DELETE FROM report WHERE post_id = :id");
    $q1->bindParam(':id', $_REQUEST['id']);
    $q1->execute();

    echo "<div class='notify'> <div class='alert alert-success'>Success, Post was deleted successfully!</div>";

    echo "<center><a href='post'><i class='fa fa-refresh fa-5x fa-spin'></i></a><br>Please click to refresh page!</center> </div>";

    ?>
    <script> setTimeout(function() { window.location.replace('post');}, 2000); </script>
    <?php
    }else{
    echo "<div class='notify'> <div class='alert alert-danger'>Error, Post failed to delete.</div>";

    echo "<center><a href='post'><i class='fa fa-refresh fa-5x fa-spin'></i></a><br>Please click to refresh page!</center></div>";
  }

  }

if(isset($_REQUEST['postid'])){
  $q = $conn->prepare("SELECT * FROM post WHERE id = :postid");
  $q->bindParam(':postid', $_REQUEST['postid']);

  $q->execute();

  $row = $q->fetch();
  ?>

  <style type="text/css"> #image{ width: auto !important;}</style>
  <!--Image-->
  <?php if($row['type'] == 'image'){ ?>
    <img src="../<?php echo $row['source']; ?>" class="cover overlay cover-image-full hover" style="width: 100%; max-height: 400px">
  <?php } ?>

  <!--Video-->
  <?php if($row['type'] == 'video'){ ?>
  <span id="span" class="icon-block width-100">
  <?php
    if(isset($row['source'])){
    ?>
    <video height=240 width=320>
      <source src="../<?php echo $row['source']?>" type="video/mp4"></source>
      Your browser does not support the video tag.
    </video>
    <?php
    }else{
       echo '<i class="fa fa-film text-light" class="icon-block width-100 bg-grey-100"></i>';
    }
  ?>
  </span>
  <?php } ?>

  <!--Category-->
  <div class="form-group category">
    <label>Post Category</label>
    <select class="form-control" disabled>
      <option><?php echo $row['category']?></option>
    </select>
  </div>
  <!--Title-->
  <div class="form-group title">
    <label>Post Title</label>
    <input class="form-control" value="<?php echo $row['title'];?>" disabled/>
  </div>
  <!--Description-->
  <div class="form-group description">
    <label for="description">Description</label>
    <textarea cols="30" rows="5" class="form-control" disabled><?php echo $row['description'];?></textarea>
  </div> 
  <!---->

  <button type="button" style="width: 48%;" class="btn btn-danger" id="del_user_post" ref="<?php echo $_REQUEST['postid']; ?>">
    Delete
  </button>

  <button type="button" class="btn btn-primary"  style="width: 48%; float: right;" data-dismiss="modal">Cancel</button>
  <?php
  }
  ?>

<script type="text/javascript">
  $('#del_user_post').click(function(){
    var id = $('#del_user_post').attr('ref');
    
    $('#staff_post_delete').load('include/delete_post.php .notify',{'id': id});
  });
</script>

