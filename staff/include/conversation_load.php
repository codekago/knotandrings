<?php

include('../../include/connect.php');

                  $user = secureTxt($_SESSION['logged_staff']);

                  function limit_words($string, $word_limit)
                      {
                          $words = explode(" ",$string);
                          return implode(" ",array_splice($words,0,$word_limit));
                      }

                  $q = $conn->prepare("SELECT * FROM staff_conversation WHERE sender = :user OR receiver = :user ORDER BY timestamp DESC");
                  $q->bindParam(':user', $user);
                  $q->execute();

                  $q4 = $conn->prepare("SELECT * FROM staff_conversation WHERE sender = :user OR receiver = :user ORDER BY timestamp DESC LIMIT 1");
                  $q4->bindParam(':user', $user);
                  $q4->execute();

                  $row4 = $q4->fetch();
                  $id4 = $row4['conversation_id'];

                  if ($q->rowCount() != 0) {
                   
while ($row = $q->fetch()) {
   
   $id = $row['conversation_id'];
  ?>
<li class="list-group-item msg-list <?php
if ($id == $id4) {
  echo "active";
}
?>" id="<?php echo $id; ?>">
                      <a href="javascript:;">
                        <div class="media v-middle">
                          <div class="media-left">
                            <img <?php
if ($row['sender'] == $user) {
  $us = $row['receiver'];
  $ft = $conn->prepare("SELECT * FROM staff WHERE username = :us");
  $ft->bindParam(':us', $us);
  $ft->execute();

  while ($row2 = $ft->fetch()) {
    if ($row2['photo'] == '') {
      ?>
src="uploads/profile/avatar.png"
    <?php
    }else{
    ?>
src="<?php echo $row2['photo']; ?>"
    <?php
  }
}//end of while loop

}else{
  $us = $row['sender'];
  $ft = $conn->prepare("SELECT * FROM staff WHERE username = :us");
  $ft->bindParam(':us', $us);
  $ft->execute();

  while ($row2 = $ft->fetch()) {
    if ($row2['photo'] == '') {
      ?>
src="uploads/profile/avatar.png"
    <?php
    }else{
    ?>
src="<?php echo $row2['photo']; ?>"
    <?php
  }
}//end of while loop
}
                            ?> width="50" height="50" alt="" class="media-object" />
                          </div>
                          <div class="media-body">
                            <span class="date"><?php echo timeAgo($row['timestamp']) ?></span>
                            <span class="user"><?php
if ($row['sender'] == $user) {
  echo $row['receiver'];
  $_SESSION['receiver'] = $row['receiver'];
}else{
  echo $row['sender'];
  $_SESSION['receiver'] = $row['sender'];
}
                            ?></span>
                            <div class="text-light"><?php
                            $ruth = $conn->prepare("SELECT * from staff_message WHERE conversation_id = :id ORDER BY id DESC");
                            $ruth->bindParam(':id', $id);
                            $ruth->execute();
                            $contnt = $ruth->fetch();

$content =  $contnt['message'];

                  if (str_word_count($content) > 5) {
                    echo limit_words($content,5)."...";
                  }else{
                    echo $content;
                  }
                            ?></div>
                          </div>
                        </div>
                      </a>
                    </li>
  <?php
}

                  }else{
                    ?>
<div class="alert alert-info">
<strong>You do not have any message</strong>
</div>
                    <?php
                  }

                            ?>
                  <script>
$('#conversationLoader').css('display', 'none');
$('.msg-list').click(function() {

  $('#sendBtn, #userMessage').removeAttr('disabled');
  $('#userMessage').focus();

$('.msg-list').removeClass('active');
$(this).addClass('active');

var user = $(this).attr('id');

localStorage.setItem('msgUser', user);

$('#messagesLoader').css('display', 'block');

$('#messageLoad').load('include/message_load.php', {'user': user});
    });
</script>