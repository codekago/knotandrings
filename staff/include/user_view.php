<script>
$('body').oLoader('hide');
</script>
<?php

include('../../include/connect.php');

$id = secureTxt($_REQUEST['id']);

$q = $conn->prepare("SELECT * FROM staff WHERE id = :id");
$q->bindParam(':id', $id);

$q->execute();
$row = $q->fetch();
$user = $row['username'];

?>
<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $row['username']; ?> profile</h4>
      </div>
      <div class="modal-body">
      <div class="row">
  <div class="col-md-4">
  	<img src="<?php echo $row['photo']; ?>" style="" class="img-thumbnail">
  </div>
  <div class="col-md-8">
  	<p><b>Name: </b><?php echo $row['name']; ?></p>
  	<p><b>Email address: </b><?php echo $row['email']; ?></p>
  	<p><b>Phone: </b><?php echo $row['phone']; ?></p>
  	<p><b>Address: </b><?php echo $row['address']; ?></p>
  	<p><b>Gender: </b><?php echo $row['name']; ?></p>
  	<p><b>Account creation date: </b><?php echo $row['acct_date']; ?></p>
  	<p><b>Staff role: </b><?php echo $row['role']; ?></p>
  </div>
</div>
<h4>Staff logs</h4>
<?php
$q = $conn->prepare("SELECT * FROM staff_logs WHERE username = :user ORDER BY id DESC LIMIT 5");
$q->bindParam(':user', $user);

$q->execute();

if ($q->execute()) {
	
?>
<table class="table">
      <thead>
        <tr>
          <th>Ip address</th>
          <th>Date</th>
          <th>Time</th>
        </tr>
      </thead>
      <tbody>
      <?php
while ($row = $q->fetch()) {
	?>

        <tr>
          <td><?php echo $row['ip_address']; ?></td>
          <td><?php echo $row['date']; ?></td>
          <td><?php echo $row['time']; ?></td>
        </tr>
	<?php
}

?>

      </tbody>
    </table>
    <?php
}else{
	?>
<div class="alert alert-info">
<strong>Staff have not logged into Knotandrings.</strong>
</div>
	<?php
}
?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
<script>
$('#viewModal').modal('show');
</script>
<?php