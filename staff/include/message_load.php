<?php

include('../../include/connect.php');

if (isset($_REQUEST['user'])) {
	$id = secureTxt($_REQUEST['user']);

$q = $conn->prepare("SELECT * FROM staff_message WHERE conversation_id = :id ORDER BY id DESC");
$q->bindParam(':id', $id);
$q->execute();
}else{
	$q3 = $conn->prepare("SELECT * FROM staff_conversation ORDER BY timestamp DESC LIMIT 1");
$q3->execute();

$st = $q3->fetch();
$stamp = $st['conversation_id'];

	$q = $conn->prepare("SELECT * FROM staff_message WHERE conversation_id = :id ORDER BY id DESC");
$q->bindParam(':id', $stamp);
$q->execute();
}


if ($q->rowCount() != 0) {
	
while ($row = $q->fetch()) {
	$user = $row['sender'];
	?>
<div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                <div class="panel-body">
                  <div class="media v-middle">
                    <div class="media-left">
                      <img <?php

$df = $conn->prepare("SELECT * FROM staff WHERE username = :user");
$df->bindParam(':user', $user);
$df->execute();

while ($gh = $df->fetch()) {
	?>
src="<?php echo $gh['photo']; ?>"
		<?php
	}

                      ?> alt="user photo" class="media-object img-circle width-50" style="height: 50px;" />
                    </div>
                    <div class="media-body message">
                      <h4 class="text-subhead margin-none"><a href="user_view?username=<?php echo $row['sender']; ?>"><?php echo $row['sender']; ?></a></h4>
                      <p class="text-caption text-light"><i class="fa fa-clock-o"></i> <?php echo timeAgo($row['time']) ?></p>
                    </div>
                  </div>
                  <p><?php echo $row['message']; ?></p>
                </div>
              </div>
	<?php
}//end of while loop

}else{
	?>
<div class="alert alert-warning">
<strong>You do not have any message with this staff</strong>
</div>
	<?php
}
?>
<script>
$('#messagesLoader').css('display', 'none');
</script>