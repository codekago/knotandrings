<?php

include('../../include/connect.php');

$user = secureTxt($_REQUEST['user']);
$q = $conn->prepare("SELECT * FROM staff WHERE username = :user");
$q->bindParam(':user', $user);
$q->execute();
$row = $q->fetch();

?>
<form class="form-horizontal" action='<?php echo htmlspecialchars('staff?ref=list');?>' method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>
          <div class="col-md-6">
            <div class="media v-middle">
              <div class="media-left">
                <div class="icon-block width-100 bg-grey-100">
                  <output id="list" style="padding-top: 0px;">
                    <span id="span">
                        <img <?php echo 'src="'.$row['photo'].'"'; ?> alt="<?php echo $user; ?>" class="" style="height: 100px; width: 100px;" id="image" />
                        
                    </span>
                  </output>
                </div>
              </div>
              <div class="media-body">
                <input type="file" value="Add Image" accept="image/*" id="files" name="image" class="btn btn-white btn-sm paper-shadow relative" />
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="inputEmail3" class="col-md-2 control-label">Full Name</label>
          <div class="col-md-6">
            <div class="form-control-material">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-male"></i></span>
                
                <input type="text" class="form-control" id="exampleInputFirstName" value="<?php echo $row['name']; ?>" placeholder="Your Full Name" required autofocus name="name">
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="inputEmail3" class="col-md-2 control-label">Email</label>
          <div class="col-md-6">
            <div class="form-control-material">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                
                <input type="email" class="form-control" value="<?php echo $row['email']; ?>" id="inputEmail3" placeholder="Email" required name="email">
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="inputAddress" class="col-md-2 control-label">Address</label>
          <div class="col-md-6">
            <div class="form-control-material">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                <input type="text" class="form-control" value="<?php echo $row['address']; ?>" id="inputAddress" placeholder="Address" required name="address">
              </div>
            </div>
          </div>
        </div>


        <div class="form-group">
          <label for="inputPassword3" class="col-md-2 control-label">Phone Number</label>
          <div class="col-md-6">
            <div class="form-control-material">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
             
                <input type="text" class="form-control" name="phone" value="<?php echo $row['phone']; ?>" id="inputPassword3" placeholder="Phone Number" required>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="inputPassword3" class="col-md-2 control-label">Gender</label>
          <div class="col-md-6">
            <div class="form-control-material">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <select class="form-control" name="gender">
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                </select>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group margin-none">
          <div class="col-md-offset-2 col-md-10">
            <button type="submit" class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Save Changes</button>
          </div>
        </div>
      </form>
      <hr>
<script>
$('body').oLoader('hide');
</script>