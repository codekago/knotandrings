<?php 
	include('../../include/connect.php');

	$id = $_REQUEST['post_id'];

	$q = $conn->prepare("SELECT * FROM post WHERE id = :id");
  	$q->bindParam(':id', $id);
  	$q->execute();
  	$row = $q->fetch();

  	if($row['status'] == 0){
  		$q = $conn->prepare("UPDATE post SET status = '1' WHERE id = :id");
	  	$q->bindParam(':id', $id);
	  	$q->execute();
?>
<button type="button" class="btn btn-danger btn-lg paper-shadow relative" data-z="1" data-hover-z="2" data-animated onclick="endorsement(<?php echo $row['id'] ?>);">Unendorse</button>
<?php
 	}else{
 		$q = $conn->prepare("UPDATE post SET status = '0' WHERE id = :id");
	  	$q->bindParam(':id', $id);
	  	$q->execute();
?>
<button type="button" class="btn btn-success btn-lg paper-shadow relative" data-z="1" data-hover-z="2" data-animated onclick="endorsement(<?php echo $row['id'] ?>);">Endorse</button>
<?php
  	}
?>