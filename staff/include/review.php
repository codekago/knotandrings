<script>
$('body').oLoader('hide');
</script>
<?php

include('../../include/connect.php');

$title = secureTxt($_REQUEST['title']);
$desc = secureTxt($_REQUEST['desc']);
$role = secureTxt($_REQUEST['role']);
$user = secureTxt($_REQUEST['user']);
$id = secureTxt($_REQUEST['id']);
$rate = secureTxt($_REQUEST['rate']);

$q = $conn->prepare("INSERT INTO rating (username, title, role, post_id, rate, review) VALUES (:user, :title, :role, :post_id, :rate, :description)");
$q->bindParam(':user', $user);
$q->bindParam(':title', $title);
$q->bindParam(':role', $role);
$q->bindParam(':post_id', $id);
$q->bindParam(':rate', $rate);
$q->bindParam(':description', $desc);

if ($q->execute()) {
	?>
<div class="alert alert-success">
<strong>Post have been successfully reviewed</strong>
</div>
<script>
$('#rate-title').val();
	var desc = $('#rate-description, #rate-role, #rate-user, #rate-id').val('');
	
localStorage.setItem('rate-number', '');
setTimeout(function() {
window.location.reload();
}, 2500);
</script>
	<?php
}else{
	?>
<div class="alert alert-warning">
<strong>Unable to review post at the moment</strong>
</div>
	<?php
}
