<?php

include('../../include/connect.php');

$user = secureTxt($_REQUEST['user']);
$type = secureTxt($_REQUEST['type']);

if ($type == 'suspend') {
	$update = $conn->prepare("UPDATE staff SET status = 1 WHERE username = :user");
	$update->bindParam(':user', $user);
	if ($update->execute()) {
		?>
<div class="alert alert-success">
<strong>Staff account suspended</strong><br>reloading page...
</div>
<script>
setTimeout(function() {
window.location.reload();
}, 2000);
</script>
		<?php
	}else{
?>
<div class="alert alert-danger">
<strong>Staff account unable to suspend</strong>
</div>
		<?php
	}

}elseif ($type == 'unsuspend') {
	$update = $conn->prepare("UPDATE staff SET status = 0 WHERE username = :user");
	$update->bindParam(':user', $user);
	if ($update->execute()) {
		?>
<div class="alert alert-success">
<strong>Staff account unsuspended</strong><br>reloading page...
</div>
<script>
setTimeout(function() {
window.location.reload();
}, 2000);
</script>
		<?php
	}else{
?>
<div class="alert alert-danger">
<strong>Staff account unable to unsuspend</strong>
</div>
		<?php
	}

}

?>
<script>
$('body').oLoader('hide');
</script>