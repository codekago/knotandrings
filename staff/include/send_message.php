<?php

include('../../include/connect.php');

$msg = secureTxt($_REQUEST['msg']);
$sender = secureTxt($_SESSION['logged_staff']);
$receiver = secureTxt($_REQUEST['receiver']);
$timestamp = time();

$q = $conn->prepare("SELECT * FROM staff_conversation WHERE sender = :sender AND receiver = :receiver OR receiver = :sender2 AND sender = :receiver2");
                  $q->bindParam(':sender', $sender);
                  $q->bindParam(':receiver', $receiver);
                  $q->bindParam(':sender2', $sender);
                  $q->bindParam(':receiver2', $receiver);
                  $q->execute();


        $w = $q->fetch();
		$id = $w['conversation_id'];


                  if ($q->rowCount() != 0) {

                  	$qe = $conn->prepare("INSERT INTO staff_message (conversation_id, sender, message, date, time) VALUES (:id, :sender, :msg, :d, :t)");
					$qe->bindParam(':id', $id);
					$qe->bindParam(':sender', $sender);
					$qe->bindParam(':msg', $msg);
					$qe->bindParam(':d', $d);
					$qe->bindParam(':t', $timestamp);


					$update = $conn->prepare("UPDATE staff_conversation SET date = :d, time = :t, timestamp = :time WHERE sender = :sender AND receiver = :receiver OR sender = :sender2 AND receiver = :receiver2");
					$update->bindParam(':d', $d);
					$update->bindParam(':t', $t);
					$update->bindParam(':time', $timestamp);
					$update->bindParam(':sender', $receiver);
					$update->bindParam(':sender2', $sender);
					$update->bindParam(':receiver', $sender);
					$update->bindParam(':receiver2', $receiver);

					if ($qe->execute() && $update->execute()) {
						?>
<div class="alert alert-success" id="msgAlert2">
Message sent!
</div>
<script>
$('#userMessage').val('');
$('#messageLoad').load('include/message_load.php', {'user': "<?php echo $receiver; ?>"});
    $('#conversation_load').load('include/conversation_load.php', {'userwe': "<?php echo $receiver; ?>"});
    setTimeout(function() {
$('#msgAlert2').hide('slow');
    }, 2000);
</script>
	<?php
}else{
?>
<div class="alert alert-danger">
Message not sent!
</div>
	<?php
}

                  }else{

					$id = rand('1674300000', '9999999999');

					$q = $conn->prepare("INSERT INTO staff_conversation (conversation_id, sender, receiver, date, time, timestamp) VALUES (:id, :sender, :receiver, :d, :t, :time)");
					$q->bindParam(':id', $id);
					$q->bindParam(':sender', $sender);
					$q->bindParam(':receiver', $receiver);
					$q->bindParam(':d', $d);
					$q->bindParam(':t', $t);
					$q->bindParam(':time', $timestamp);

					$q2 = $conn->prepare("INSERT INTO staff_message (conversation_id, sender, message, date, time) VALUES (:id, :sender, :msg, :d, :t)");
					$q2->bindParam(':id', $id);
					$q2->bindParam(':sender', $sender);
					$q2->bindParam(':msg', $msg);
					$q2->bindParam(':d', $d);
					$q2->bindParam(':t', $timestamp);

					if ($q->execute() && $q2->execute()) {
						?>
<div class="alert alert-success" id="msgAlert2">
Message sent!
</div>
<script>
$('#userMessage').val('');
$('#messageLoad').load('include/message_load.php', {'user': "<?php echo $receiver; ?>"});
    $('#conversation_load').load('include/conversation_load.php', {'userwe': "<?php echo $receiver; ?>"});
    setTimeout(function() {
$('#msgAlert2').hide('slow');
    }, 2000);
</script>
	<?php
}else{
?>
<div class="alert alert-danger">
Message not sent!
</div>
	<?php
}

                  }

?>
<script>
$('body').oLoader('hide');
$('#message').val('');
</script>