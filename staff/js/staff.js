$(document).ready(function() {

	function loader() {
        $('body').oLoader({
            wholeWindow: true, //makes the loader fit the window size
            lockOverflow: true, //disable scrollbar on body

            backgroundColor: '#000',
            fadeInTime: 1000,
            fadeLevel: 0.4,
            image: '../img/loader.gif',
            //hideAfter: 1500
        });
    }//end of loader function

    $('.view').click(function() {
    	var id = $(this).attr('id');

    	loader();

    	$('#userLoad').load('include/user_view.php', {'id': id});
    });


$('.1').click(function() {
  $('.1').removeClass('fa-star-o');
  $('.2, .3, .4, .5').addClass('fa-star-o');
  $('.1').addClass('fa-star');
  $('#rate-status').text("poor");

  localStorage.setItem('rate-number', '1');
});

$('.2').click(function() {
  $('.1, .2').removeClass('fa-star-o');
  $('.1, .2').addClass('fa-star');
  $('.3, .4, .5').addClass('fa-star-o');
  $('#rate-status').text("okay");

  localStorage.setItem('rate-number', '2');
});

$('.3').click(function() {
  $('.1, .2, .3').removeClass('fa-star-o');
  $('.4, .5').addClass('fa-star-o');
  $('.1, .2, .3').addClass('fa-star');
  $('#rate-status').text("good");

  localStorage.setItem('rate-number', '3');
});

$('.4').click(function() {
  $('.1, .2, .3, .4').removeClass('fa-star-o');
  $('.5').addClass('fa-star-o');
  $('.1, .2, .3, .4').addClass('fa-star');
  $('#rate-status').text("awesome");

  localStorage.setItem('rate-number', '4');
});

$('.5').click(function() {
  $('.1, .2, .3, .4, .5').removeClass('fa-star-o');
  $('.1, .2, .3, .4, .5').addClass('fa-star');
  $('#rate-status').text("It's excellent");

  localStorage.setItem('rate-number', '5');
});

$('#rate-form').submit(function() {
	var title = $('#rate-title').val();
	var desc = $('#rate-description').val();
	var role = $('#rate-role').val();
	var user = $('#rate-user').val();
	var id = $('#rate-id').val();
	var rate = localStorage.getItem('rate-number');


	if (rate != '') {
	loader();
		$('#rate-alert').load('include/review.php', {'title': title, 'desc': desc, 'role': role, 'user': user, 'id': id, 'rate': rate});
	}else{
		alert('You have not rated this post yet.');
	}

return false;
});

});