<?php

include('../include/connect.php');
include('../include/connect2.php');

if (isset($_SESSION['logged_staff'])) {
   $page = "view";

   $user = $_SESSION['logged_staff'];

   $q = $conn->prepare("SELECT * FROM staff WHERE username = :user");
   $q->bindParam(':user', $user);
   $q->execute();

   $row = $q->fetch();


   $email = $row['email'];
  $address = $row['address'];
  $photo = $row['photo'];
   $name = $row['name'];
   $gender = $row['gender'];
   $img = $row['photo'];
   $phone = $row['phone'];
   $role = $row['role'];
   $admin = $row['admin'];

}else{

if($_GET['page'] != ''){
    $pages = array("login");
    if(in_array($_GET['page'], $pages)){
        $page = $_GET['page'];
        
    }else{
  $page="login";
}
}else{
    $page="login";
}

}//end of login check
?>
<!DOCTYPE html>
<html class="full" lang="en">
<!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Knot and Rings</title>

        <?php
if (isset($_SESSION['logged_staff'])) {
   ?>
<link href="../user/css/vendor/all.css" rel="stylesheet">
<link href="../user/css/app/app.css" rel="stylesheet">
<link href="../libraries/dropzone/basic.min.css" rel="stylesheet">
   <?php
}else{
?>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    
    <link href="../libraries/fonts/font-awesome.min.css" rel="stylesheet" />
   <link href="../user/css/vendor/all.css" rel="stylesheet">
<link href="../user/css/app/app.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php
}

?>
    
 
</head>

<body     <?php
if (isset($_SESSION['logged_staff'])) {
   ?>

   <?php
}else{
?>
class="login"
<?php
}
?>
>

<?php require('pages/'.$page.'.php'); ?>


    <?php
if (isset($_SESSION['logged_staff'])) {
   ?>
   <script src="../js/jquery.js"></script>
   <script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.masonry.min.js"></script>
   <script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "html",
      skins: {
        "default": {
          "primary-color": "#42a5f5"
        }
      }
    };


    //--Staff Endorsment--//
    function endorsement(e){
      var post_id = e
      $('#endorsment').load('include/endorsment.php',{'post_id': post_id});
    }

    //Deleting of users post
    function delete_user_post(e){
      var post_id = e
      $('#staff_post_delete').load('include/delete_post.php',{'postid': post_id});
    }
    
  </script>
  <script src="../user/js/vendor/all.js"></script>
<script src="../user/js/app/app.js"></script>
<script src="../libraries/dropzone/dropzone.min.js"></script>
<script src="js/staff.js"></script>
<script src="js/imageUpload.js"></script>
        <script src="../js/jquery.oLoader.js"></script> 
   <?php
}else{
?>
<!-- jQuery -->
    <script src="../js/jquery.js"></script>
<script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "html",
      skins: {
        "default": {
          "primary-color": "#42a5f5"
        }
      }
    };

    //--Staff Endorsment--//
    function endorsement(e){
      var post_id = e;
      $('#endorsment').load('include/endorsment.php',{'post_id': post_id});
    }

    //Deleting of users post
    function delete_user_post(e){
      var post_id = e;
      $('#staff_post_delete').load('include/delete_post.php',{'postid': post_id});
    }

  </script>

  <script src="../user/js/vendor/all.js"></script>
<script src="../user/js/app/app.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>
    
<?php
}
    ?>

    
    
  <!--Delete_user_post_modal-->
<div class="modal fade" id="delete_user_post_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content" style="background-color: #FAFCFC;">
      <div class="modal-body">
        <h3>Delete Post</h3>
        <!--Load content-->
        <div id="staff_post_delete" style="clear:both;"><img src="../img/loader.gif" style="margin: 40px auto 40px 45%"></div>
      </div>
    </div>
    </div>
  </div>
</div>
<!--End of edit post modal-->
</body>

</html>
