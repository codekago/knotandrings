

<div class="container">

    <div class="page-section">
      <div class="row">

        <div class="col-md-3">
        <?php require('pages/sidemenu.php'); ?>
        </div>
        
        <div class="col-md-9">

          <div class="row" data-toggle="isotope">
           
            <div class="item col-xs-12 col-lg-7">
              <div class="panel panel-default paper-shadow" data-z="0.5">
                <div class="panel-heading">
                  <?php
                    $q = $conn->prepare("SELECT * FROM report");
                    $q->execute();
                  ?>
                  <h4 class="text-headline">Reports <small>(<?php echo $q->rowCount();?>)</small></h4>
                </div>
                 <ul class="list-group relative paper-shadow" data-hover-z="0.5" data-animated>
                    <?php

                      while($row = $q->fetch()){

                        $q2 = $conn->prepare("SELECT * FROM post WHERE id = :id");
                        $q2->bindParam(':id', $row['post_id']);
                        $q2->execute();

                        $post_details = $q2->fetch();
                    ?>

                    <li class="list-group-item paper-shadow">
                    <div class="media v-middle">
                      <div class="media-left">
                        <a href="\knotandrings-copy/staff/post?ref=post_view&id=<?php echo $post_details['id']; ?>">
                          <?php if($post_details['type'] == "image"){ ?>
                          <img src="../<?php echo $post_details['source']?>" alt="post" class="width-60 height-50" />
                          <?php }else{?>
                          <i class="fa fa-fw fa-film fa-4x"></i>
                          <?php } ?>
                        </a>
                      </div>
                      <div class="media-body">
                        <a href="\knotandrings-copy/staff/post?ref=post_view&id=<?php echo $post_details['id']; ?>" class="text-subhead link-text-color"><?php echo $row['report']; ?></a>
                        <div class="text-light">
                          Post by: <a href=""><?php echo $post_details['username']; ?></a> &nbsp; Report By: <a href="#"><?php echo $row['username']; ?></a>
                        </div>
                      </div>
                      <div class="media-right">
                        <div class="width-120 text-right">
                          <span class="text-caption text-light"><?php echo $row['date']; ?></span>
                          <div class="icon-block img-circle bg-red-300" style="width:20px; height:20px; line-height: 5px">
                            <a data-toggle="modal" data-target="#delete_user_post_modal" onclick="delete_user_post(<?php echo $post_details['id']; ?>);" style="font-size: 11px; cursor:pointer;">                              
                              <i class="fa fa-trash text-white" style="font-size: 11px;"></i>
                            </a>
                        </div>
                        </div>
                      </div>
                    </div>
                    </li>

                    <?php
                    }
                    ?>
                </ul>
              </div>
            </div>

             <div class="item col-xs-12 col-lg-5">
              <div class="panel panel-default paper-shadow" data-z="0.5">
                <div class="panel-body">
                  <h4 class="text-headline margin-none">Rewards</h4>
                  <p class="text-subhead text-light">Your latest achievements</p>
                  <div class="icon-block half img-circle bg-purple-300">
                    <i class="fa fa-star text-white"></i>
                  </div>
                  <div class="icon-block half img-circle bg-indigo-300">
                    <i class="fa fa-trophy text-white"></i>
                  </div>
                  <div class="icon-block half img-circle bg-green-300">
                    <i class="fa fa-mortar-board text-white"></i>
                  </div>
                  <div class="icon-block half img-circle bg-orange-300">
                    <i class="fa fa-code-fork text-white"></i>
                  </div>
                  <div class="icon-block half img-circle bg-red-300">
                    <i class="fa fa-diamond text-white"></i>
                  </div>
                </div>
              </div>
              <div class="panel panel-default paper-shadow" data-z="0.5">
                <div class="panel-heading">
                  <h4 class="text-headline">Certificates
                    <small>(4)</small>
                  </h4>
                </div>
                <div class="panel-body">
                  <a class="btn btn-default text-grey-400 btn-lg btn-circle paper-shadow relative" data-hover-z="0.5" data-animated data-toggle="tooltip" data-title="Name of Certificate">
                    <i class="fa fa-file-text"></i>
                  </a>
                  <a class="btn btn-default text-grey-400 btn-lg btn-circle paper-shadow relative" data-hover-z="0.5" data-animated data-toggle="tooltip" data-title="Name of Certificate">
                    <i class="fa fa-file-text"></i>
                  </a>
                  <a class="btn btn-default text-grey-400 btn-lg btn-circle paper-shadow relative" data-hover-z="0.5" data-animated data-toggle="tooltip" data-title="Name of Certificate">
                    <i class="fa fa-file-text"></i>
                  </a>
                  <a class="btn btn-default text-grey-400 btn-lg btn-circle paper-shadow relative" data-hover-z="0.5" data-animated data-toggle="tooltip" data-title="Name of Certificate">
                    <i class="fa fa-file-text"></i>
                  </a>
                </div>
              </div>
            </div>

          </div>

        </div>

    </div>
  </div>

</div>