<div class="tab-content" style="min-height: 300px;">

              <div id="account" class="tab-pane active table-responsive">
<table class="table table-striped">
<h2>List of users</h2>
      <thead>
        <tr>
          <th>Photo</th>
          <th>Username</th>
          <th>Name</th>
          <th>Email Address</th>
          <th>Phone Number</th>
          <th>Date of birth</th>
          <th>Gender</th>
          
        </tr>
      </thead>
      <tbody>
      <?php
      $logged_staff = secureTxt($_SESSION['logged_staff']);

$q = $conn->prepare("SELECT * FROM profile ORDER BY id DESC");
$q->execute();

while ($row = $q->fetch()) {
  ?>
<tr>
          <td><img src="../<?php echo $row['image']; ?>" style="height: 40px; width: 40px;" /></td>
          <td><?php echo $row['username']; ?></td>
          <td><?php echo $row['name']; ?></td>
          <td><?php echo $row['email']; ?></td>
          <td><?php echo $row['phone']; ?></td>
          <td><?php echo $row['dob']; ?></td>
          <td><?php echo $row['gender']; ?></td>
        </tr>
  <?php
}
      ?>
        
      </tbody>
    </table>
              </div>

            </div>