<!-- Tabbable Widget -->
<div class="tabbable paper-shadow relative" data-z="0.5">
  <!-- Tabs -->
  <ul class="nav nav-tabs">
    <li <?php if (isset($_GET['ref'])) { if ($_GET['ref'] == 'view') {  echo 'class="active"';  }  }else{  echo 'class="active"';  }  ?>>
      <a href="profile?ref=view" title="View Profile" style="cursor: pointer;">
        <i class="fa fa-fw fa-eye"></i>
        <span class="hidden-sm hidden-xs">View Profile</span>
      </a>
    </li>

    <li <?php if (isset($_GET['ref'])) { if ($_GET['ref'] == 'edit') { echo 'class="active"'; } } ?>>
      <a href="profile?ref=edit" title="Edit Profile" style="cursor: pointer;">
        <i class="fa fa-fw fa-pencil-square-o"></i> 
        <span class="hidden-sm hidden-xs">Edit Profile</span>
      </a>
    </li>

    <li <?php if (isset($_GET['ref'])) { if ($_GET['ref'] == 'password') { echo 'class="active"'; } }else{echo 'class="active"'; } ?>>
      <a href="profile?ref=password" title="Change Password" style="cursor: pointer;">
        <i class="fa fa-fw fa-lock"></i> 
        <span class="hidden-sm hidden-xs">Change Password</span>
      </a>
    </li>
    </ul>
  <!-- // END Tabs -->

  <!-- Panes -->
  <div class="tab-content">
    <div id="account" class="tab-pane active">
      <?php
        if(isset($_POST['name'])) {

          $name = secureTxt($_POST['name']);
          $email = secureTxt($_POST['email']);
          $address = secureTxt($_POST['address']);
          $gender = secureTxt($_POST['gender']);
          $phone = secureTxt($_POST['phone']);

          if(!empty($_FILES["image"]["tmp_name"])) {
                $target_dir = "uploads/profile/";
                $target_file = $target_dir . basename($_FILES["image"]["name"]);
                $uploadOk = 1;
                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                
                // Check if image file is a actual image or fake image
                if(isset($_POST["name"])) {
                    @$check = getimagesize($_FILES["image"]["tmp_name"]);
                    if($check !== false) {
                        
                        $uploadOk = 1;
                    } else {
                        echo "<div class='alert alert-warning'>File is not a valid image.</div>";
                        $uploadOk = 0;
                    }
                }
                // Check if file already exists
                if (file_exists($target_file)) {
                  echo "<div class='alert alert-warning'>Sorry, photo already exists.</div>";
                  $uploadOk = 0;
                }
                // Check file size
                if ($_FILES["image"]["size"] > 500000) {
                    echo "<div class='alert alert-warning'>Sorry, your photo is too large.</div>";
                    $uploadOk = 0;
                }
                // Allow certain file formats
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
                    echo "<div class='alert alert-warning'>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</div>";
                    $uploadOk = 0;
                }
                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                    echo "<div class='alert alert-danger'>Sorry, your photo can not be uploaded.</div>";
                // if everything is ok, try to upload file
                } else {
                  $q = $conn->prepare("UPDATE staff SET name = :name,  email = :email, address = :address, phone = :phone, gender = :gender, photo = :image WHERE username = :user");

                  $q->bindParam(':name', $name);
                  $q->bindParam(':email', $email);
                  $q->bindParam(':address', $address);
                  $q->bindParam(':phone', $phone);
                  $q->bindParam(':gender', $gender);
                  $q->bindParam(':user', $user);
                  $q->bindParam(':image', $target_file);

                
                  if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file) && $q->execute()) {
                    //delete old image file
                    unlink($photo);
                  ?>
                      <div class="alert alert-success">
                        <strong>Your profile have been updated successfully.</strong><br>
                        Refreshing your profile...
                      </div>
                      <script>
                        setTimeout(function() { window.location.replace('profile?ref=view');}, 2000);
                      </script>
                      <?php
                  } else {
                      echo "<div class='alert alert-danger'>Error code 1: Sorry, there was an error updating your profile.</div>";
                  }
                }
          }else{
            
            $q = $conn->prepare("UPDATE staff SET name = :name, email = :email, address = :address, phone = :phone, gender = :gender WHERE username = :user");

            $q->bindParam(':name', $name);
            $q->bindParam(':email', $email);
            $q->bindParam(':address', $address);
            $q->bindParam(':phone', $phone);
            $q->bindParam(':gender', $gender);
            $q->bindParam(':user', $user);

          
            if ($q->execute()) {
                ?>
                <div class="alert alert-success">
                  <strong>Your profile has been updated successfully.</strong><br>
                  Refreshing your profile...
                </div>
                <script>
                  setTimeout(function() { window.location.replace('profile?ref=view');}, 2000);
                </script>
                <?php
            } else {
              echo "<div class='alert alert-danger'>Error code 2: Sorry, there was an error updating your profile.</div>";
            }
          }
        }
      ?>

      <form class="form-horizontal" action='<?php echo htmlspecialchars('profile?ref=edit');?>' method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>
          <div class="col-md-6">
            <div class="media v-middle">
              <div class="media-left">
                <div class="icon-block width-100 bg-grey-100">
                  <output id="list" style="padding-top: 0px;">
                    <span id="span"><?php if ($row['photo'] == '') {  ?>
                    <img <?php echo 'src="images/user.png"'; ?> alt="<?php echo $user; ?>" style="height: 100px; width: 100px;" id="image" />
                    <?php
                      }else{
                        ?>
                        <img <?php echo 'src="'.$row['photo'].'"'; ?> alt="<?php echo $user; ?>" class="" style="height: 100px; width: 100px;" id="image" />
                        <?php
                      }
                    ?>
                    </span>
                  </output>
                </div>
              </div>
              <div class="media-body">
                <input type="file" value="Add Image" accept="image/*" id="files" name="image" class="btn btn-white btn-sm paper-shadow relative" />
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="inputEmail3" class="col-md-2 control-label">Full Name</label>
          <div class="col-md-6">
            <div class="form-control-material">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-male"></i></span>
                <?php if ($row['email'] == '') { ?>
                <input type="text" class="form-control" id="exampleInputFirstName" placeholder="Your Full name" required name="name">
                <?php 
                }else{
                ?>
                <input type="text" class="form-control" id="exampleInputFirstName" value="<?php echo $name; ?>" placeholder="Your Full Name" required autofocus name="name">
                <?php
                }
                ?>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="inputEmail3" class="col-md-2 control-label">Email</label>
          <div class="col-md-6">
            <div class="form-control-material">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <?php if ($row['email'] == '') { ?>
                <input type="email" class="form-control" id="inputEmail3" placeholder="Email" required name="email">
                <?php 
                }else{
                ?>
                <input type="email" class="form-control" value="<?php echo $email; ?>" id="inputEmail3" placeholder="Email" required name="email">
                <?php
                }
                ?> 
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="inputAddress" class="col-md-2 control-label">Address</label>
          <div class="col-md-6">
            <div class="form-control-material">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                <?php if ($row['address'] == '') { ?>
                <input type="text" class="form-control" id="inputAddress" placeholder="Address" required name="address">
                <?php 
                }else{
                ?>
                <input type="text" class="form-control" value="<?php echo $address; ?>" id="inputAddress" placeholder="Address" required name="address">
                <?php
                }
                ?> 
              </div>
            </div>
          </div>
        </div>


        <div class="form-group">
          <label for="inputPassword3" class="col-md-2 control-label">Phone Number</label>
          <div class="col-md-6">
            <div class="form-control-material">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <?php
                if ($row['email'] == '') {
                ?>
                <input type="text" class="form-control" name="phone" id="inputPassword3" placeholder="Phone Number" required>
                <?php 
                }else{
                ?>
                <input type="text" class="form-control" name="phone" value="<?php echo $phone; ?>" id="inputPassword3" placeholder="Phone Number" required>
                <?php
                }
                ?>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="inputPassword3" class="col-md-2 control-label">Gender</label>
          <div class="col-md-6">
            <div class="form-control-material">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <select class="form-control" name="gender">
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                </select>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group margin-none">
          <div class="col-md-offset-2 col-md-10">
            <button type="submit" class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Save Changes</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- // END Panes -->

</div>
<!-- // END Tabbable Widget -->