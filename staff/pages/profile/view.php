<!-- Tabbable Widget -->
<div class="tabbable paper-shadow relative" data-z="0.5">
  <!-- Tabs -->
  <ul class="nav nav-tabs">
    <li <?php if (isset($_GET['ref'])) { if ($_GET['ref'] == 'view') {  echo 'class="active"';  }  }else{  echo 'class="active"';  }  ?>>
      <a href="profile?ref=view" title="View Profile" style="cursor: pointer;">
        <i class="fa fa-fw fa-eye"></i>
        <span class="hidden-sm hidden-xs">View Profile</span>
      </a>
    </li>

    <li <?php if (isset($_GET['ref'])) { if ($_GET['ref'] == 'edit') { echo 'class="active"'; } } ?>>
      <a href="profile?ref=edit" title="Edit Profile" style="cursor: pointer;">
        <i class="fa fa-fw fa-pencil-square-o"></i> 
        <span class="hidden-sm hidden-xs">Edit Profile</span>
      </a>
    </li>

    <li <?php if (isset($_GET['ref'])) { if ($_GET['ref'] == 'password') { echo 'class="active"'; } }else{echo 'class="active"'; } ?>>
      <a href="profile?ref=password" title="Change Password" style="cursor: pointer;">
        <i class="fa fa-fw fa-lock"></i> 
        <span class="hidden-sm hidden-xs">Change Password</span>
      </a>
    </li>
    </ul>
  <!-- // END Tabs -->


  <!-- Panes -->
  <div class="tab-content">

    <div id="account" class="tab-pane active">
      <?php  if ($name == '') {  ?>
      <div class="alert alert-warning">
        <strong>You have not yet created your profile.</strong>
        <a href="profile?ref=edit" title="Create account here">Create profile</a> 
      </div>
      <?php
      }else{
      ?>
      <form class="form-horizontal">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>
          <div class="col-md-6">
            <div class="media v-middle">
              <div class="media-left">
                <div class="icon-block width-100 bg-grey-100">
                  <?php  if ($row['photo'] == '') {  ?>
                  <img <?php echo 'src="images/user.png"'; ?> alt="<?php echo $user; ?>" style="height: 100%; width: 100%;" id="image" />
                  <?php
                  }else{
                  ?>
                  <img <?php echo 'src="'.$row['photo'].'"'; ?> alt="<?php echo $user; ?>" class="" style="height: 100%; width: 100%;" id="image" />
                  <?php
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="inputEmail3" class="col-md-2 control-label">Name:</label>
              <div class="col-md-6">
               <div class="form-control-material">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-male"></i></span>
                    <span><?php echo $name; ?></span>
                  </div>
                </div>
              </div>
        </div>

        <div class="form-group">
          <label for="inputEmail3" class="col-md-2 control-label">Email:</label>
          <div class="col-md-6">
            <div class="form-control-material">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <span><?php echo $email; ?></span>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="inputEmail3" class="col-md-2 control-label">Address</label>
          <div class="col-md-6">
            <div class="form-control-material">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                <span><?php echo $address; ?></span>
              </div>
              </div>
            </div>
        </div>

        <div class="form-group">
          <label for="inputPassword3" class="col-md-2 control-label">Phone Number</label>
          <div class="col-md-6">
            <div class="form-control-material">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <span><?php echo $phone; ?></span>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="inputPassword3" class="col-md-2 control-label">Gender</label>
          <div class="col-md-6">
            <div class="form-control-material">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <span><?php echo $gender; ?></span>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group margin-none">
          <div class="col-md-offset-2 col-md-10">
            <a href="profile?ref=password" class="btn btn-primary paper-shadow relative" >Change Password</a>
          </div>
        </div>
      </form>
      <?php  }//user with account to view  ?>
    </div>

  </div>
  <!-- // END Panes -->

</div>
<!-- // END Tabbable Widget -->