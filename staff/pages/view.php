<?php

if (!isset($_SESSION['logged_staff'])) {
  header('location: login');
}

if($_GET['page'] != ''){
    $pages = array("staff", "user", "profile", "dashboard", "post", "messages", "logout", "video", "post", "terms", "policy");
    if(in_array($_GET['page'], $pages)){
        $page = $_GET['page'];
        
    }else{
  $page="post";
}
}else{
    $page="post";
}

?>

<!-- Fixed navbar -->
  <div class="navbar navbar-default navbar-fixed-top navbar-size-large navbar-size-xlarge paper-shadow" data-z="0" data-animated role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="navbar-brand navbar-brand-logo">
          <a href="post">
            <img src="../img/logo.png" height="50px" style="" />
          </a>
        </div>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="main-nav">
        <ul class="nav navbar-nav navbar-nav-margin-left">
        <li <?php
if ($page == 'post') {
  echo 'class="active"';
}
        ?>><a href="post">Post</a></li>
        <?php
if ($role == 'admin') {
  ?>
<li <?php
if ($page == 'staff') {
  echo 'class="active"';
}
        ?>><a href="staff">Staff</a></li>
  <?php
}
        ?>
        <li <?php
if ($page == 'user') {
  echo 'class="active"';
}
        ?>><a href="user">User</a></li>
        <li <?php
if ($page == 'dashboard' || $page == 'profile' || $page == 'messages') {
  echo 'class="active"';
}
        ?>><a href="dashboard">My Account</a></li>
        </ul>
        <div class="navbar-right">
          <ul class="nav navbar-nav navbar-nav-bordered navbar-nav-margin-right">
            <!-- user -->
            <li class="dropdown user active">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img <?php
if ($row['photo'] == '') {
  echo 'src="images/user.png"';
}else{
  $photo = $row['photo'];
  echo 'src="'.$row['photo'].'"';
}
                ?> alt="" class="img-circle" style="height: 30px;" /> <?php echo $_SESSION['logged_staff']; ?><span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li <?php
if ($page == 'dashboard') {
  echo 'class="active"';
}
        ?>><a href="dashboard"><i class="fa fa-bar-chart-o"></i> Dashboard</a></li>
                <li <?php
if ($page == 'post') {
  echo 'class="active"';
}
        ?>><a href="post"><i class="fa fa-bullhorn"></i> Post</a></li>
                <li <?php
if ($page == 'profile') {
  echo 'class="active"';
}
        ?>><a href="profile"><i class="fa fa-user"></i> Profile</a></li>
                <li <?php
if ($page == 'messages') {
  echo 'class="active"';
}
        ?>><a href="messages"><i class="fa fa-envelope"></i> Messages</a></li>
                <li><a href="logout"><i class="fa fa-sign-out"></i> Logout</a></li>
              </ul>
            </li>
            <!-- // END user -->
          </ul>
          <a href="logout" class="navbar-btn btn btn-primary">Logout</a>
        </div>
      </div>
      <!-- /.navbar-collapse -->

    </div>
  </div>

<!--admin header-->
<?php
if (isset($_GET['ref']) && $_GET['ref'] == 'post_view') {
  
if (isset($_GET['id'])) {
  $id = $_GET['id'];

  $q = $conn->prepare("SELECT * FROM post WHERE id = :id");
  $q->bindParam(':id', $id);
  $q->execute();

  $row = $q->fetch();

  $title = $row['title'];
  $desc = $row['description'];
  $user = $row['username'];
  $cat = $row['category'];
  $type = $row['type'];
  $img = $row['source'];
  $status = $row['status'];
}
?>
<div class="parallax overflow-hidden bg-blue-400 page-section third">
    <div class="parallax-layer container" data-opacity="true">
      <div class="media v-middle">
        <div class="media-left">
          <span class="icon-block s60"><img src="../<?php echo $img; ?>" style="width: 100%; height: 100%" /></span>
        </div>
        <div class="media-body">
          <h1 class="text-display-1 margin-none"><?php echo $title; ?></h1>
          <p class="small margin-none" onclick="$('#reviewModal').modal('show');">
          <span class="fa fa-fw fa-star-o text-yellow-800 1" style="cursor: pointer;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800 2" style="cursor: pointer;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800 3" style="cursor: pointer;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800 4" style="cursor: pointer;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800 5" style="cursor: pointer;"></span>
          </p>
        </div>
        <div class="media-right">
          <a class="btn btn-white" href="#reviewModal" data-toggle="modal">Rate post</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
        <form role="form" id="rate-form">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: none;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <center>
<img class="img-rounded" src="<?php echo $photo; ?>" style="height: 50px;" /> <h4 class="modal-title" id="myModalLabel">Review by <?php echo $_SESSION['logged_staff']; ?></h4>

<p class="small margin-none">
            <span class="fa fa-fw fa-star-o text-yellow-800 1" style="color: #F58634; cursor: pointer;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800 2"style="color: #F58634; cursor: pointer;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800 3"style="color: #F58634; cursor: pointer;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800 4"style="color: #F58634; cursor: pointer;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800 5"style="color: #F58634; cursor: pointer;"></span>
</p>
<p id="rate-status" style="color: #F58634; margin-bottom: 0px;"></p>

        </center>
        
      </div>
      <div class="modal-body">
        <center>
        <p id="rate-alert"></p>
        </center>
          <div class="form-group">
<label>Title</label>
<input type="hidden" id="rate-user" value="<?php echo $_SESSION['logged_staff']; ?>" />
<input type="hidden" id="rate-role" value="<?php echo $role; ?>" />
<input type="hidden" id="rate-id" value="<?php echo $id; ?>" />
<input type="text" required id="rate-title" class="form-control" />
          </div>
          <div class="form-group">
<label>Description</label>
<textarea id="rate-description" required class="form-control" row="3"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

        </form>
  </div>
</div>
<?php

}else{
  ?>
<div class="parallax overflow-hidden bg-blue-400 page-section third">
    <div class="container parallax-layer" data-opacity="true">
      <div class="media v-middle">
        <div class="media-left text-center">
          <a href="#">
            <img <?php
if ($row['photo'] == '') {
  echo 'src="uploads/profile/avatar.png"';
}else{
  echo 'src="'.$row['photo'].'"';
}
                ?> alt="<?php echo $user; ?>" class="img-circle width-80" style="height: 80px;" />
          </a>
        </div>
        <div class="media-body">
          <?php 
if ($name == '') {
  ?>
  <h1 class="text-white text-display-1 margin-v-0">
  <?php echo $_SESSION['logged_staff']; ?>
  </h1>
          <p class="text-subhead">You don't have a profile with us. <a class="link-white text-underline" href="profile?ref=edit">Create profile</a></p>
        </div>
  <?php
}else{
?>
<h1 class="text-white text-display-1 margin-v-0">
  <?php echo $_SESSION['logged_staff']; ?>
  </h1>
          <em><a class="link-white" href="profile?ref=view">Welcome <?php echo $_SESSION['logged_staff']; ?>!</a></em>
        </div>
  <?php
}
           ?>
      </div>
    </div>
  <?php
}
?>
  


  </div>
  

<?php require('pages/'.$page.'.php'); ?>

<!--Message Modal-->
<div class="modal fade bs-example-modal-sm" id="msgModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content alert-info" style="background-color: #d9edf7 !important; border-color: #bce8f1 !important;">
      <form role="form" id="msgForm">
<div class="modal-header" style="border-bottom: none; padding-bottom: 5px;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Message to <span id="msgUser"></span></h4>
        <p id="msgAlert"></p>
      </div>
      <div class="modal-body" style="padding-top: 0px; padding-bottom: 0px;">
        <div class="form-group">
<textarea id="message" required placeholder="Write your message" row="3" class="form-control"></textarea>
        </div>
      </div>
      <div class="modal-footer" style="border-top: none; padding-top: 5px;">
        <button type="submit" class="btn btn-info">Submit</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!--edit staff Modal-->
<div class="modal fade bs-example-modal-sm" id="editModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content alert-info" style="background-color: #FFEFD2 !important; border-color: #FFEFD2 !important;">
<div class="modal-header" style="border-bottom: none; padding-bottom: 5px;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit profile</h4>
       
      </div>
      <div class="modal-body" style="padding-top: 0px; padding-bottom: 0px;">
        Are you sure you want to edit <span id="editUser"></span> profile?
      </div>
      <div class="modal-footer" style="border-top: none; padding-top: 5px;">
        <button type="submit" class="btn btn-info" id="yesBtn" data-dismiss="modal">Yes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <!-- Footer -->
  <footer class="footer">
    <strong>Knot and Rings</strong> &copy; Copyright <?php echo date('Y'); ?>
  </footer>
  <!-- // Footer -->
  <div id="top" title="Top"><i class="fa fa-arrow-up"></i></div>
  <style>
#top {
  position: fixed;
  right: 20px;
  bottom: 20px;
  width: 24px;
  height: 24px;
  background-size: 100%;
  z-index: 9999999999;
  opacity: 0.2;
}
  </style>
