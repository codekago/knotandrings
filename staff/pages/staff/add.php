<!-- Panes -->
            <div class="tab-content">

              <div id="account" class="tab-pane active">
                <?php
if (isset($_POST['user'])) {
  
  $name = secureTxt($_POST['name']);
  $user = secureTxt($_POST['user']);
  $pwd = securePwd($_POST['pwd']);
  $address = secureTxt($_POST['address']);
  $gender = secureTxt($_POST['gender']);
  $role = secureTxt($_POST['role']);
  $email = secureTxt($_POST['email']);
  $phone = secureTxt($_POST['phone']);

$target_dir = "uploads/profile/";
$target_file = $target_dir . basename($_FILES["image"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["name"])) {
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if($check !== false) {
        
        $uploadOk = 1;
    } else {
        echo "<div class='alert alert-warning'>File is not an image.</div>";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "<div class='alert alert-warning'>Sorry, photo already exists.</div>";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["image"]["size"] > 500000) {
    echo "<div class='alert alert-warning'>Sorry, your photo is too large.</div>";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "<div class='alert alert-warning'>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</div>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "<div class='alert alert-danger'>Sorry, your photo can not be uploaded.</div>";
// if everything is ok, try to upload file
} else {
    $q = $conn->prepare("INSERT INTO staff (name, username, password, email, phone, address, photo, gender, acct_date, role) VALUES (:name, :user, :pwd, :email, :phone, :address, :photo, :gender, :d, :role)");
    $q->bindParam(':name', $name);
    $q->bindParam(':user', $user);
    $q->bindParam(':pwd', $pwd);
    $q->bindParam(':email', $email);
    $q->bindParam(':phone', $phone);
    $q->bindParam(':address', $address);
    $q->bindParam(':photo', $target_file);
    $q->bindParam(':gender', $gender);
    $q->bindParam(':d', $d);
    $q->bindParam(':role', $role);

    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file) && $q->execute()) {
?>
<div class="alert alert-success">
<strong>Staff have been successfully added</strong>
</div>
<?php
    }else{
?>
<div class="alert alert-danger">
<strong>Staff have not been successfully added</strong>
</div>
<?php
    }

}
//adding of staff
}
                ?>
<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars('staff?ref=add'); ?>" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>
                    <div class="col-md-6">
                      <div class="media v-middle">
                        <div class="media-left">
                          <div class="icon-block width-100 bg-grey-100">
                            <output id="list" style="padding-top: 0px;"><span id="span">
  <img src="uploads/profile/avatar.png" alt="User photo" class="" style="height: 100px; width: 100px;" id="image" />
  </span></output>
                          </div>
                          <input type="file" value="Add Image" required accept="image/*" id="files" name="image" class="btn btn-white btn-sm paper-shadow relative" />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">Username:</label>
                        <div class="col-md-6">
                         <div class="form-control-material">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                          <span><input type="text" name="user" required class="form-control" placeholder="Staff username" value"<?php
if (isset($_POST['user'])) {
  echo $_POST['user'];
}
                          ?>" autofocus /></span>
                        </div>
                      </div>
                        </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">Password:</label>
                        <div class="col-md-6">
                         <div class="form-control-material">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                          <span><input type="password" required class="form-control" placeholder="Staff password" name="pwd"  /></span>
                        </div>
                      </div>
                        </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">Name:</label>
                        <div class="col-md-6">
                         <div class="form-control-material">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-male"></i></span>
                          <span><input type="text" required class="form-control" placeholder="Staff name" name="name" value"<?php
if (isset($_POST['user'])) {
  echo $_POST['name'];
}
                          ?>" /></span>
                        </div>
                      </div>
                        </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">Email:</label>
                    <div class="col-md-6">
                      <div class="form-control-material">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                          <span><input type="text" name="email" required class="form-control" placeholder="Staff email" value"<?php
if (isset($_POST['user'])) {
  echo $_POST['email'];
}
                          ?>" /></span>
                        </div>
                      </div>
                    </div>
                  </div>
                                    <div class="form-group">
                    <label for="inputPassword3" class="col-md-2 control-label">Phone Number</label>
                    <div class="col-md-6">
                      <div class="form-control-material">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                          <span><input type="text" required class="form-control" placeholder="Staff phone number" value"<?php
if (isset($_POST['user'])) {
  echo $_POST['phone'];
}
                          ?>" name="phone" /></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-md-2 control-label">Gender</label>
                    <div class="col-md-6">
                      <div class="form-control-material">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-female"></i></span>
                          <span>
                            <select name="gender" class="form-control">
<option value="male">Male</option>
<option value="female">Female</option>
                            </select>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-md-2 control-label">Address</label>
                    <div class="col-md-6">
                      <div class="form-control-material">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                          <span>
                            <textarea name="address" required class="form-control" row="3"><?php
if (isset($_POST['user'])) {
  echo $_POST['user'];
}
                          ?></textarea>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-md-2 control-label">Staff role</label>
                    <div class="col-md-6">
                      <div class="form-control-material">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-group"></i></span>
                          <span>
                            <select name="role" class="form-control">
<option value="staff">Staff</option>
<option value="admin">Admin</option>
                            </select>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group margin-none">
                    <div class="col-md-offset-2 col-md-10">
                      <button type="submit" class="btn btn-primary paper-shadow relative" >Add staff</button>
                    </div>
                  </div>
                </form>
              </div>

            </div>
            <!-- // END Panes -->