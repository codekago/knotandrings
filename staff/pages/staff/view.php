<!-- Panes -->
            <div class="tab-content">

              <div id="account" class="tab-pane active">
                <?php
if ($name == '') {
  ?>
<div class="alert alert-warning">
<strong>You have not yet created your profile.</strong>
<a href="profile?ref=edit" title="Create account here">Create profile</a> 
</div>
  <?php
}else{

?>
<form class="form-horizontal">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>
                    <div class="col-md-6">
                      <div class="media v-middle">
                        <div class="media-left">
                          <div class="icon-block width-100 bg-grey-100">
                            <?php
if ($row['image'] == '') {
  ?>
  <img <?php echo 'src="images/user.png"'; ?> alt="<?php echo $user; ?>" style="height: 100%; width: 100%;" id="image" />
  <?php
}else{
  ?>
  <img <?php echo 'src="'.$row['image'].'"'; ?> alt="<?php echo $user; ?>" class="" style="height: 100%; width: 100%;" id="image" />
  <?php
}
                ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">Name:</label>
                        <div class="col-md-8">
                         <div class="form-control-material">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-male"></i></span>
                          <span><?php echo $name; ?></span>
                        </div>
                      </div>
                        </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">Email:</label>
                    <div class="col-md-6">
                      <div class="form-control-material">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                          <span><?php echo $email; ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-md-2 control-label">Website</label>
                    <div class="col-md-6">
                      <div class="form-control-material">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-link"></i></span>
                          <span><?php echo $web; ?></span>
                        </div>
                        </div>
                      </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-md-2 control-label">Phone Number</label>
                    <div class="col-md-6">
                      <div class="form-control-material">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                          <span><?php echo $phone; ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-md-2 control-label">Date of Birth</label>
                    <div class="col-md-6">
                      <div class="form-control-material">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <span><?php echo $dob; ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-md-2 control-label">Gender</label>
                    <div class="col-md-6">
                      <div class="form-control-material">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-user"></i></span>
                          <span><?php echo $gender; ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group margin-none">
                    <div class="col-md-offset-2 col-md-10">
                      <a href="profile?ref=password" class="btn btn-primary paper-shadow relative" >Change Password</a>
                    </div>
                  </div>
                </form>
<?php

}//user with account to view
                ?>
              </div>

            </div>
            <!-- // END Panes -->