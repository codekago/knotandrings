<!-- Panes -->
            <div class="tab-content" style="min-height: 300px;">

              <div id="account" class="tab-pane active table-responsive">
              <?php
        if(isset($_POST['name'])) {

          $name = secureTxt($_POST['name']);
          $email = secureTxt($_POST['email']);
          $address = secureTxt($_POST['address']);
          $gender = secureTxt($_POST['gender']);
          $phone = secureTxt($_POST['phone']);

          if(!empty($_FILES["image"]["tmp_name"])) {
                $target_dir = "uploads/profile/";
                $target_file = $target_dir . basename($_FILES["image"]["name"]);
                $uploadOk = 1;
                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                
                // Check if image file is a actual image or fake image
                if(isset($_POST["name"])) {
                    @$check = getimagesize($_FILES["image"]["tmp_name"]);
                    if($check !== false) {
                        
                        $uploadOk = 1;
                    } else {
                        echo "<div class='alert alert-warning'>File is not a valid image.</div>";
                        $uploadOk = 0;
                    }
                }
                // Check if file already exists
                if (file_exists($target_file)) {
                  echo "<div class='alert alert-warning'>Sorry, photo already exists.</div>";
                  $uploadOk = 0;
                }
                // Check file size
                if ($_FILES["image"]["size"] > 500000) {
                    echo "<div class='alert alert-warning'>Sorry, your photo is too large.</div>";
                    $uploadOk = 0;
                }
                // Allow certain file formats
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
                    echo "<div class='alert alert-warning'>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</div>";
                    $uploadOk = 0;
                }
                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                    echo "<div class='alert alert-danger'>Sorry, your photo can not be uploaded.</div>";
                // if everything is ok, try to upload file
                } else {
                  $q = $conn->prepare("UPDATE staff SET name = :name,  email = :email, address = :address, phone = :phone, gender = :gender, photo = :image WHERE username = :user");

                  $q->bindParam(':name', $name);
                  $q->bindParam(':email', $email);
                  $q->bindParam(':address', $address);
                  $q->bindParam(':phone', $phone);
                  $q->bindParam(':gender', $gender);
                  $q->bindParam(':user', $user);
                  $q->bindParam(':image', $target_file);

                
                  if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file) && $q->execute()) {
                    //delete old image file
                    unlink($photo);
                  ?>
                      <div class="alert alert-success">
                        <strong>Your profile have been updated successfully.</strong><br>
                        Refreshing your profile...
                      </div>
                      <script>
                        setTimeout(function() { window.location.replace('profile?ref=view');}, 2000);
                      </script>
                      <?php
                  } else {
                      echo "<div class='alert alert-danger'>Error code 1: Sorry, there was an error updating your profile.</div>";
                  }
                }
          }else{
            
            $q = $conn->prepare("UPDATE staff SET name = :name, email = :email, address = :address, phone = :phone, gender = :gender WHERE username = :user");

            $q->bindParam(':name', $name);
            $q->bindParam(':email', $email);
            $q->bindParam(':address', $address);
            $q->bindParam(':phone', $phone);
            $q->bindParam(':gender', $gender);
            $q->bindParam(':user', $user);

          
            if ($q->execute()) {
                ?>
                <div class="alert alert-success">
                  <strong>Your profile has been updated successfully.</strong><br>
                  Refreshing your profile...
                </div>
                <script>
                  setTimeout(function() { window.location.replace('staff?ref=list');}, 2000);
                </script>
                <?php
            } else {
              echo "<div class='alert alert-danger'>Error code 2: Sorry, there was an error updating your profile.</div>";
            }
          }
        }
      ?>
              <div id="editLoad"></div>
                <table class="table table-striped">
      <thead>
        <tr>
          <th>Photo</th>
          <th>Username</th>
          <th>Email Address</th>
          <th>Phone Number</th>
          <th>Role</th>
          <th>Action</th>
          
        </tr>
      </thead>
      <tbody>
      <?php
      $logged_staff = secureTxt($_SESSION['logged_staff']);

$q = $conn->prepare("SELECT * FROM staff WHERE username != :user ORDER BY id DESC");
$q->bindParam(':user', $logged_staff);
$q->execute();

while ($row = $q->fetch()) {
  ?>
<tr>
          <td><a href="javascript:;" class="view" id="<?php echo $row['id']; ?>"><img src="<?php echo $row['photo']; ?>" style="height: 40px; width: 40px;" /></a></td>
          <td><a href="javascript:;" class="view" id="<?php echo $row['id']; ?>"><?php echo $row['username']; ?></a></td>
          <td><?php echo $row['email']; ?></td>
          <td><?php echo $row['phone']; ?></td>
          <td><?php echo $row['role']; ?></td>
          <td><?php
if ($role == 'admin') {
  ?>
<span class='editBtn' id="<?php echo $row['username']; ?>"><button class="btn btn-info" title="Edit staff">Edit</button></span> <?php
if ($row['status'] == 0) {
  ?>
<span class='suspendBtn' id="<?php echo $row['username']; ?>"><button class="btn btn-danger" title="Suspend staff">Suspend</button></span>
  <?php
}else{
  ?>
<span class='unsuspendBtn' id="<?php echo $row['username']; ?>"><button class="btn btn-success" title="Suspend staff">Unsuspend</button></span>
  <?php
}
?>
  <?php
}
          ?> <span class='msgBtn' id="<?php echo $row['username']; ?>"><button class="btn btn-warning" title="Message staff" data-toggle="modal" data-target="msgModal">Message</button></span></td>
        </tr>
  <?php
}
      ?>
        
      </tbody>
    </table>
              </div>

            </div>
            
            <!-- // END Panes -->
<script src="../js/jquery.js"></script>
<script>
$('document').ready(function() {
  function loader() {
        $('body').oLoader({
            wholeWindow: true, //makes the loader fit the window size
            lockOverflow: true, //disable scrollbar on body

            backgroundColor: '#000',
            fadeInTime: 1000,
            fadeLevel: 0.4,
            image: '../img/loader.gif',
            //hideAfter: 1500
        });
    }//end of loader function

$('.msgBtn').click(function() {
  $('#msgModal').modal('show');
  var user = $(this).attr('id');
      $('#msgUser').text(user);
});

$('.suspendBtn').click(function() {
  var user = $(this).attr('id');
  loader();
      $('#editLoad').load('include/suspention.php', {'user': user, 'type': 'suspend'});
});

$('.unsuspendBtn').click(function() {
  var user = $(this).attr('id');
  loader();
      $('#editLoad').load('include/suspention.php', {'user': user, 'type': 'unsuspend'});
});

$('.editBtn').click(function() {
  var user = $(this).attr('id');
      $('#editUser').text(user);
  $('#editModal').modal('show');
});

$('#yesBtn').click(function() {
    loader();
var user = $('#editUser').text();
$('#editLoad').load('include/editUser.php', {'user': user});
});

$('#msgForm').submit(function() {
var msg = $('#message').val();
var user = $('#msgUser').text();

$('#msgAlert').load('include/send_message.php', {'receiver': user, 'msg': msg});

return false;
    });

});
</script>
