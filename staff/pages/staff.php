
  <div class="container">

    <div class="page-section">
      <div class="row">

        <div class="col-md-3">
<?php require('pages/staffmenu.php'); ?>
</div>

        <div class="col-md-9">

        <!-- Tabbable Widget -->
          <div class="tabbable paper-shadow relative" data-z="0.5">

            <!-- Tabs -->
            <ul class="nav nav-tabs">
              <li <?php
if (isset($_GET['ref'])) {

      if ($_GET['ref'] == 'list') {
      echo 'class="active"';
    }

}else{
  echo 'class="active"';
}
              ?>><a href="staff?ref=list" title="Staff list" style="cursor: pointer;"><i class="fa fa-fw fa-list"></i> <span class="hidden-sm hidden-xs">Staff list</span></a></li>
              <li <?php
              if (isset($_GET['ref'])) {
  
      if ($_GET['ref'] == 'add') {
        echo 'class="active"';
      }

}
              ?>><a href="staff?ref=add" title="Add staff" style="cursor: pointer;"><i class="fa fa-fw fa-plus"></i> <span class="hidden-sm hidden-xs">Add staff</span></a></li>
            </ul>
            <!-- // END Tabs -->


          <?php

if (isset($_GET['ref'])) {
  
    if($_GET['ref'] != ''){
        $ref = array("view", "list", "add", "edit", "delete");
        if(in_array($_GET['ref'], $ref)){
            $ref = $_GET['ref'];
            
        }else{
      $ref = "list";
    }
    }else{
        $ref = "list";
    }

}else{
  $ref = "list";
}
  
require('pages/staff/'.$ref.'.php');

?>


          </div>
          <!-- // END Tabbable Widget -->
        </div>

      </div>
    </div>

  </div>

  <!-- view staff modal -->
<!-- Modal -->
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <span id="userLoad"></span>
</div>