<?php
$session_destroy = session_destroy();

if ($session_destroy) {
	header("location: login");
}