<h3>Image post</h3>
<form action="<?php echo htmlspecialchars('post?ref=add&type=image'); ?>" method="post" enctype="multipart/form-data"><br>
  <div class="form-group">
    <label>Upload Image</label>
      <div class="media v-middle">
        <div class="media-left">
          <div class="icon-block width-100 bg-grey-100">
          <output id="list" class="icon-block width-100 bg-grey-100" style="padding-top: 0px;"><span id="span" class="icon-block width-100 bg-grey-100">
            <i class="fa fa-photo text-light" class="icon-block width-100 bg-grey-100"></i>
            </span></output>
          </div>
        </div>
        <div class="media-body">
        <input type="file" value="Add Image" required accept="image/*" id="files" name="image" class="btn btn-white btn-sm paper-shadow relative" />
        <input type="hidden" name="imagepost" value="image" />
        </div>
      </div>
  </div>
  <div class="form-group">
    <label>Post Category</label>
    <select class="form-control" name="category">
      <option value="White_wedding">White wedding</option>
      <option value="Traditional_marriage">Traditional marriage</option>
      <option value="Accessories_designers">Accessories designers</option>
      <option value="Master_of_ceremonies">Master of ceremonies (mc)</option>
      <option value="Photographers">Photographers</option>
      <option value="Decorators">Decorators</option>
      <option value="Disc_jockey">Disc jockey (dj)</option>
      <option value="Cake_and_desert">Cake and desert</option>
      <option value="Catering_and_drinks">Catering and drinks</option>
      <option value="Bridal_couture">Bridal couture</option>
      <option value="souveniers">Souveniers</option>
    </select>
  </div>
  <div class="form-group">
    <label>Post Title</label>
    <input type="text" name="title" id="title" placeholder="Title of post" class="form-control" value="<?php
  if (isset($_POST['title']) && $uploadOk == 0) {
  echo $_POST['title'];
  }
    ?>" />
  </div>
  <div class="form-group">
    <label for="description">Description</label>
    <textarea name="description" id="description" cols="30" rows="5" class="form-control" placeholder="Description of post"><?php
  if (isset($_POST['title']) && $uploadOk == 0) {
  echo $_POST['description'];
  }
    ?></textarea>
  </div>
  <div class="text-right">
  <input type="submit" class="btn btn-primary" />
  </div>
</form>