<div class="row grid js-masonry" data-masonry-options='{ "itemSelector": ".grid-item", "columWidth": 200 }' data-toggle="isotope">
 <div class="item col-xs-12 col-sm-6 col-lg-4 grid-item">
    <div class="panel panel-default paper-shadow" data-z="0.5">

      <div class="cover overlay cover-image-full hover">
        <span class="img icon-block height-150 bg-grey-200"></span>
        <a href="post?ref=add&type=image" class="view-post">
          <img src="../img/logo.png" class="cover overlay cover-image-full hover" style="width: 100%; max-height: 400px">
        </a>

      </div>

      <div class="panel-body">
      <a href="post?ref=add&type=image" style="text-align: center;">
        <h4 class="text-headline margin-v-0-10">
          Add Post
        </h4>
      </a>
    </div>
    
    <hr class="margin-none" />
    <div class="panel-body">
        <div class="col-sm-12 col-lg-6">
          <a class="btn btn-default btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="post?ref=add&type=image" style="font-size: 11px;">
          <div class="icon-block img-circle bg-green-300" style="width:20px; height:20px; line-height: 5px">
            <i class="fa fa-image text-white" style="font-size: 11px;"></i>
          </div>
          Picture
        </a>
        </div>
        <div class="col-sm-12 col-lg-6">
          <a class="btn btn-default btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="post?ref=add&type=video" style="font-size: 11px;">
          <div class="icon-block img-circle bg-blue-300" style="width:20px; height:20px; line-height: 5px">
            <i class="fa fa-film text-white" style="font-size: 11px;"></i>
          </div>
          Video
        </a>
        </div>

    </div>

    </div>
  </div>
<?php

$q = $conn->prepare("SELECT * FROM post ORDER BY id DESC");

if ($q->execute()) {

  if (empty($row['username'])) {
    ?>
<div class="alert alert-warning">
<strong>No new post to be reviewed.</strong>
</div>
    <?php
  }else{
?>

<?php

while ($row = $q->fetch()) {
  ?>
<div class="item col-xs-12 col-sm-6 col-lg-4 grid-item">
  <div class="panel panel-default paper-shadow" data-z="0.5">
    <a href="post?ref=post_view&id=<?php echo $row['id']; ?>"class="view-post">
    
    <?php if($row['type'] == 'image'){ ?>
    <!--if post is an image-->
    <img src="../<?php echo $row['source']; ?>" class="cover overlay cover-image-full hover" style="width: 100%; max-height: 400px">
    <?php }else{ ?>
    <!--if post is a video-->
    <div class="embed-responsive embed-responsive-16by9">
      <i class="fa fa-play fa-3x video_button" style="visibility: hidden;" id='<?php echo "btn_".$row['id']; ?>' onmouseenter="show_btn(<?php echo $row['id']; ?>);" onmouseout="hide_btn(<?php echo $row['id']; ?>);"></i>
      <video class="embed-responsive-item" onmouseenter="show_btn(<?php echo $row['id']; ?>);" onmouseout="hide_btn(<?php echo $row['id']; ?>);">
        <source src="../<?php echo $row['source']?>" type="video/mp4"></source>
        Your browser does not support the video tag.
      </video>
    </div>
    <?php } ?>
    </a>

    <div class="panel-body">
      <a href="post?ref=post_view&id=<?php echo $row['id']; ?>">
        <h4 class="text-headline margin-v-0-10"><a href="post?ref=<?php echo $row['title']; ?>"><?php echo $row['title']; ?></a></h4>
      </a>
    </div>
    
    <hr class="margin-none" />
    <div class="panel-body">
        <div class="col-sm-12 col-lg-5">
          <a class="btn btn-default btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="post?ref=post_view&id=<?php echo $row['id']; ?>" style="font-size: 11px;">
          <i class="fa fa-fw fa-eye"></i> View Post
        </a>
        </div>
        <div class="col-sm-12 col-lg-5 pull-left">
          <!-- Display delete modal -->
          <button class="btn btn-danger" data-toggle="modal" data-target="#delete_user_post_modal" onclick="delete_user_post(<?php echo $row['id']; ?>);" style="font-size: 11px;"><i class="fa fa-fw fa-trash"></i> Delete post</button>
        
        </div>

    </div>
  </div>
</div>
            
  <?php
}//end of while statement....

?>
<div style="clear: both"></div>

          <?php

  }//checking the number of user post


}

?>
</div>
          