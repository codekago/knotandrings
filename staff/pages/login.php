<div id="content">
    <div class="container-fluid">

<form action="<?php echo htmlspecialchars('login');?>" method="post">
      <div class="lock-container">
      <?php
      if (isset($_POST['user'])) {
        $user = secureTxt($_POST['user']);
        $pwd = securePwd($_POST['pwd']);

        $q = $conn->prepare("SELECT * FROM staff WHERE username = :user");
        $q->bindParam(':user', $user);

        $q->execute();

        $row = $q->fetch();

        if ($row['username'] == $user && $row['password'] == $pwd) {
          $_SESSION['logged_staff'] = $row['username'];

function getIP() { 
$ip; 
if (getenv("HTTP_CLIENT_IP")) 
$ip = getenv("HTTP_CLIENT_IP"); 
else if(getenv("HTTP_X_FORWARDED_FOR")) 
$ip = getenv("HTTP_X_FORWARDED_FOR"); 
else if(getenv("REMOTE_ADDR")) 
$ip = getenv("REMOTE_ADDR"); 
else 
$ip = "UNKNOWN";
return $ip; 

} 

$ip = getIP();

          $log = $conn->prepare("INSERT INTO staff_logs (username, date, time, ip_address) VALUES (:user, :date, :time, :ip)");
                $log->bindParam(':user', $user);
                $log->bindParam(':date', $d);
                $log->bindParam(':time', $t);
                $log->bindParam(':ip', $ip);
                $log->execute();
          header('location: view');

        }else{
          ?>
<div class="alert alert-danger">
<strong>Incorrect login details</strong>
</div>
          <?php
        }

      }
      ?>
        <div class="panel panel-default text-center paper-shadow" data-z="0.5">
          <h1 class="text-display-1 text-center margin-bottom-none">Staff login</h1>
          <img src="../images/people/110/guy-5.jpg" class="img-circle width-80">
          <div class="panel-body">
            <div class="form-group">
                <label>Username</label>
                <input class="form-control" value="<?php
if (isset($_POST['user'])) {
  echo $_POST['user'];
}
                ?>" autofocus required name="user" id="username" type="text" placeholder="Username">
              </div>
            <div class="form-group">
                <label>Password</label>
                <input class="form-control" name="pwd" required id="password" type="password" placeholder="Enter Password">
              </div>

            <button type="submit" class="btn btn-primary">Login <i class="fa fa-fw fa-unlock-alt"></i></button>
            <a href="forgot-password" class="forgot-password">Forgot password?</a>
          </div>
        </div>
      </div>
      </form>

    </div>
  </div>
