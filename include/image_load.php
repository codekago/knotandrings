
<div class="row grid js-masonry">
  <?php

  include('connect.php');

  $requested_page = $_POST['page_num'];
$set_limit = (($requested_page - 1) * 10) . ",10";

$q = $conn->prepare("SELECT * FROM post ORDER BY id DESC LIMIT $set_limit");
$q->execute();


while ($row = $q->fetch()) {
  ?>
<div class="item col-xs-12 col-sm-6 col-lg-6 grid-item">
              <div class="panel panel-default paper-shadow" data-z="0.5">

                <a href="<?php echo $row['source']; ?>" id="<?php echo $row['id']; ?>" class="view-post" data-lightbox="example-set" data-title='<?php echo $row["title"]; ?><br><?php
                  $id = $row["id"];
//echo $id;
  $q3 = $conn->prepare("SELECT rate FROM rating WHERE post_id = :id");
  $q3->bindParam(":id", $id);

  $q3->execute();
  $count = $q3->rowCount();


  if ($count > 0) {
    
      /*$array[] = $row3['rate'];
      $r2 = array_sum($array);
      echo $r2;*/

      $sum = 0;
      $ids_array = array();
      while ($row3 = $q3->fetch()) {
        
   $sum+= $row3["rate"];

      }

      
$rate = round($sum/$count);
$rate2 = round($sum/$count, 1);

?>
<b>Reviews: <?php echo $count; ?>  &nbsp;
Rate: <?php echo $rate2; ?>
</b>
<?php

if ($rate == 1) {
  ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rate == 2) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rate == 3) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rate == 4) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rate == 5) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
  <?php
}else{
  ?>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}
    
  }else{
    ?>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
  }

  

?>'>
                <img src="<?php echo $row['source']; ?>" class="cover overlay cover-image-full hover" style="width: 100%; max-height: 400px">
                </a>

                
                <div class="panel-body" style="padding-top: 0px;">

                  <div class="media v-middle">
                    <div class="media-left">
                      <img src="<?php
                      $q1 = $conn->prepare("SELECT * FROM profile WHERE username = :user");
$q1->bindParam(':user', $row['username']);
$q1->execute();

while ($row2 = $q1->fetch()) {
  echo $row2['image'];
}

                      ?>" alt="profile image" class="img-circle width-40" />
                    </div>
                    <div class="media-body">
                      <h4 style="margin-bottom: 0px;"><a href="user_view?username=<?php echo $row['username']; ?>"><?php echo $row['username']; ?></a>
                        <br/>
                      </h4>
                      <b>Date:</b> <?php echo $row['date']; ?> <b>Time:</b> <?php echo $row['time']; ?>
                    </div>
                  </div>

                </div>

              </div>
            </div>
  <?php
}