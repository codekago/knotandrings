<?php

include('connect.php');

$id = secureTxt($_REQUEST['id']);
$msg = secureTxt($_REQUEST['msg']);
$logged_user = secureTxt($_SESSION['logged_user']);
$receiver = secureTxt($_SESSION['receiver']);
$timestamp = time();

$q = $conn->prepare("INSERT INTO message (conversation_id, sender, message, date, time) VALUES (:id, :user, :msg, :d, :t)");
$q->bindParam(':id', $id);
$q->bindParam(':user', $logged_user);
$q->bindParam(':msg', $msg);
$q->bindParam(':d', $d);
$q->bindParam(':t', $timestamp);

$update = $conn->prepare("UPDATE conversation SET date = :d, time = :t, timestamp = :time WHERE sender = :sender AND receiver = :receiver OR sender = :sender2 AND receiver = :receiver2");
	$update->bindParam(':d', $d);
	$update->bindParam(':t', $t);
	$update->bindParam(':time', $timestamp);
	$update->bindParam(':sender', $receiver);
	$update->bindParam(':sender2', $logged_user);
	$update->bindParam(':receiver', $logged_user);
	$update->bindParam(':receiver2', $receiver);

if ($q->execute() && $update->execute()) {
	?>
<div class="alert alert-success" id="msgAlert">
Message sent!
</div>
<script>
$('#userMessage').val('');
$('#messageLoad').load('include/message_load.php');
    $('#conversation_load').load('include/conversation_load.php');
    setTimeout(function() {
$('#msgAlert').hide('slow');
    }, 2000);
</script>
	<?php
}else{
?>
<div class="alert alert-danger">
Message not sent!
</div>
	<?php
}


?>