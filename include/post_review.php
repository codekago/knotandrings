<?php

include('connect.php');

  $id = secureTxt($_REQUEST['id']);

  $q = $conn->prepare("SELECT * FROM rating WHERE post_id = :id ORDER BY id DESC");
  $q->bindParam(':id', $id);

  $q->execute();

  $count = $q->rowCount();
  if ($count > 0) {
    while ($row = $q->fetch()) {
    ?>
<div class="item">
                  <div class="testimonial">
                    <div class="panel panel-default">
                      <div class="panel-body">
                        <p><?php echo $row['review']; ?></p>
                      </div>
                    </div>
                    <div class="media v-middle">
                      <div class="media-left">
                      <?php
if ($row['role'] == 'user') {
  $table = 'profile';

}else{
  $table = 'staff';
}

$q1 = $conn->prepare("SELECT * FROM $table WHERE username = :user");
$q1->bindParam(':user', $row['username']);
$q1->execute();

while ($row2 = $q1->fetch()) {
  ?>
<img src="<?php 
if ($table == 'profile') {
  echo $row2['image'];
}else{
  echo "staff/".$row2['photo'];
}
 ?>" alt="User image" class="img-circle width-40" />
  <?php
}
                      ?>
                        
                      </div>
                      <div class="media-body">
                        <p class="text-subhead margin-v-5-0">
                          <strong><a href="#"><?php echo $row['username']; ?><span class="text-muted"></span></a></strong>
                        </p>
                        <p class="small">
                          <?php
if ($row['rate'] == 1) {
  ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($row['rate'] == 2) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($row['rate'] == 3) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($row['rate'] == 4) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($row['rate'] == 5) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
  <?php
}
                          ?>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
    <?php
  }
  }else{
    ?>
<center>
<div class="alert alert-info">
<strong>This post have not been rated</strong><br>
Be the first to rate it.
</div>
</center>
    <?php
  }

  