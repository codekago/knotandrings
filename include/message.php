<?php

include('connect.php');

$user = secureTxt($_REQUEST['user']);
$msg = secureTxt($_REQUEST['msg']);
$logged_user = secureTxt($_SESSION['logged_user']);
$timestamp = time();

$q = $conn->prepare("SELECT * FROM conversation WHERE sender = :user AND receiver = :logged_user");
$q->bindParam(':user', $user);
$q->bindParam(':logged_user', $logged_user);
$q->execute();

//////////////////checking if conversation exist already///////////////

if ($q->rowCount() != 0) {

	///////////////sending message with the same conversation id///////////////
	$row = $q->fetch();
	$id = $row['conversation_id'];

	$send = $conn->prepare("INSERT INTO message (message, sender, conversation_id, date, time) VALUES (:msg, :sender, :id, :d, :t)");
	$send->bindParam(':msg', $msg);
	$send->bindParam(':sender', $logged_user);
	$send->bindParam(':id', $id);
	$send->bindParam(':d', $d);
	$send->bindParam(':t', $now);

	$update = $conn->prepare("UPDATE conversation SET date = :d, time = :t, timestamp = :time WHERE sender = :sender AND receiver = :receiver OR sender = :sender2 AND receiver = :receiver2");
	$update->bindParam(':d', $d);
	$update->bindParam(':t', $t);
	$update->bindParam(':time', $timestamp);
	$update->bindParam(':sender', $user);
	$update->bindParam(':sender2', $logged_user);
	$update->bindParam(':receiver', $logged_user);
	$update->bindParam(':receiver2', $user);

	if ($send->execute() && $update->execute()) {
		?>
<div class="alert alert-success" style="padding: 5px; margin-bottom: 0px;" id="msgAlertBox">
<strong>Message sent!</strong>
</div>
<script>
$('#message').val('');
setTimeout(function() {
$('#msgAlertBox').hide('slow');
}, 2000);
</script>
		<?php
	}else{
		?>
<div class="alert alert-danger" style="padding: 5px; margin-bottom: 0px;" id="msgAlertBox">
<strong>Message not sent!</strong>
</div>
<script>
setTimeout(function() {
$('#msgAlertBox').hide('slow');
}, 2000);
</script>
		<?php
	}
	//sending message

}else{


$q = $conn->prepare("SELECT * FROM conversation WHERE sender = :logged_user AND receiver = :user");
$q->bindParam(':user', $user);
$q->bindParam(':logged_user', $logged_user);
$q->execute();

if ($q->rowCount() != 0) {
	///////////////sending message with the same conversation id///////////////
	$row = $q->fetch();
	$id = $row['conversation_id'];

	$send = $conn->prepare("INSERT INTO message (message, sender, conversation_id, date, time) VALUES (:msg, :sender, :id, :d, :t)");
	$send->bindParam(':msg', $msg);
	$send->bindParam(':sender', $logged_user);
	$send->bindParam(':id', $id);
	$send->bindParam(':d', $d);
	$send->bindParam(':t', $now);

	$update = $conn->prepare("UPDATE conversation SET date = :d, time = :t, timestamp = :time WHERE sender = :sender AND receiver = :receiver OR sender = :sender2 AND receiver = :receiver2");
	$update->bindParam(':d', $d);
	$update->bindParam(':t', $t);
	$update->bindParam(':time', $timestamp);
	$update->bindParam(':sender', $user);
	$update->bindParam(':sender2', $logged_user);
	$update->bindParam(':receiver', $logged_user);
	$update->bindParam(':receiver2', $user);

	if ($send->execute() && $update->execute()) {
		?>
<div class="alert alert-success" style="padding: 5px; margin-bottom: 0px;" id="msgAlertBox">
<strong>Message sent!</strong>
</div>
<script>
$('#message').val('');
setTimeout(function() {
$('#msgAlertBox').hide('slow');
}, 2000);
</script>
		<?php
	}else{
		?>
<div class="alert alert-danger" style="padding: 5px; margin-bottom: 0px;" id="msgAlertBox">
<strong>Message not sent!</strong>
</div>
<script>
setTimeout(function() {
$('#msgAlertBox').hide('slow');
}, 2000);
</script>
		<?php
	}
	//sending message
}else{
///////start a new conversation and send message/////////////////

$id = rand('1674300000', '9999999999');

$q = $conn->prepare("INSERT INTO conversation (conversation_id, sender, receiver, date, time, timestamp) VALUES (:id, :logged_user, :user, :d, :t, :time)");
$q->bindParam(':id', $id);
$q->bindParam(':logged_user', $logged_user);
$q->bindParam(':user', $user);
$q->bindParam(':d', $d);
$q->bindParam(':t', $t);
$q->bindParam(':time', $timestamp);

$send = $conn->prepare("INSERT INTO message (message, sender, conversation_id, date, time) VALUES (:msg, :sender, :id, :d, :t)");
	$send->bindParam(':msg', $msg);
	$send->bindParam(':sender', $logged_user);
	$send->bindParam(':id', $id);
	$send->bindParam(':d', $d);
	$send->bindParam(':t', $now);

	if ($q->execute() && $send->execute()) {
		?>
<div class="alert alert-success" style="padding: 5px; margin-bottom: 0px;" id="msgAlertBox">
<strong>Message sent!</strong>
</div>
<script>
$('#message').val('');
setTimeout(function() {
$('#msgAlertBox').hide('slow');
}, 2000);
</script>
		<?php
	}else{
		?>
<div class="alert alert-danger" style="padding: 5px; margin-bottom: 0px;" id="msgAlertBox">
<strong>Message not sent!</strong>
</div>
<script>
setTimeout(function() {
$('#msgAlertBox').hide('slow');
}, 2000);
</script>
		<?php
	}

}


}//end of conversation check////////////////

?>
<script>
$('body').oLoader('hide');
</script>