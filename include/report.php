<?php

include('connect.php');

$user = secureTxt($_SESSION['logged_user']);
$report = secureTxt($_REQUEST['report']);
$post_id = secureTxt($_REQUEST['id']);

$q = $conn->prepare("SELECT * FROM report WHERE username = :user AND post_id = :id AND report = :report");
$q->bindParam(':user', $user);
$q->bindParam(':id', $post_id);
$q->bindParam(':report', $report);
$q->execute();

if ($q->rowCount() != 0) {
	?>
<div class="alert alert-success">
<strong>We have received your report and it's been processed</strong><br>
You don't have to make a double report
</div>
	<?php
}else{
	$q = $conn->prepare("INSERT INTO report (post_id, username, report, date, time, timestamp) VALUES (:id, :user, :report, :d, :t, :stamp)");
	$q->bindParam(':id', $post_id);
	$q->bindParam(':user', $user);
	$q->bindParam(':report', $report);
	$q->bindParam(':d', $d);
	$q->bindParam(':t', $t);
	$q->bindParam(':stamp', $now);

	if ($q->execute()) {
		?>
<div class="alert alert-success">
<strong>We have received your report and it's been processed</strong>
</div>
		<?php
	}else{
		?>
<div class="alert alert-warning">
<strong>Unable to send report at the moment</strong>
</div>
		<?php
	}
}

?>
<script>
$('body').oLoader('hide');
</script>