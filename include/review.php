<script>
$('body').oLoader('hide');
</script>
<?php

include('connect.php');

$title = secureTxt($_REQUEST['title']);
$desc = secureTxt($_REQUEST['desc']);
$user = secureTxt($_REQUEST['user']);
$id = secureTxt($_REQUEST['id']);
$rate = secureTxt($_REQUEST['rate']);

$q = $conn->prepare("INSERT INTO rating (username, title, post_id, rate, review) VALUES (:user, :title, :post_id, :rate, :description)");
$q->bindParam(':user', $user);
$q->bindParam(':title', $title);
$q->bindParam(':post_id', $id);
$q->bindParam(':rate', $rate);
$q->bindParam(':description', $desc);

if ($q->execute()) {
	?>
<div class="alert alert-success">
<strong>Post have been successfully reviewed</strong>
<br>Refreshing post...
</div>
<script>
$('#rate-title, #rate-description').val('');
	
localStorage.setItem('rate-number', '');
setTimeout(function() {
window.location.replace('explore?post=<?php echo $id; ?>');
}, 2000);
</script>
	<?php
}else{
	?>
<div class="alert alert-warning">
<strong>Unable to review post at the moment</strong>
</div>
	<?php
}
