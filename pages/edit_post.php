<?php
include('../include/connect.php');
include('../include/connect2.php');
$q = $conn->prepare("SELECT * FROM post WHERE id = :postid");
$q->bindParam(':postid', $_REQUEST['postid']);

$q->execute();

$row = $q->fetch();

$_SESSION['type'] = $row['type'];
$_SESSION['postid'] = $_REQUEST['postid'];
$_SESSION['source'] = $row['source'];
?>

<style type="text/css"> #image{ width: auto !important;}</style>
<!--Image-->

<?php if($row['type'] == 'image'){ ?>
<div class="form-group">
  <label>Upload Image</label>
    <div class="media v-middle">
      <div class="media-left">
        <div class="icon-block width-100">
        <output id="list" class="icon-block width-100" style="padding-top: 0px;">
          <span id="span" class="icon-block width-100">
          <?php
            if(isset($row['source'])){
            ?>
            <i style="height: 100%; width: 500px; display: block; background: url('<?php echo $row["source"]?>'); background-size: contain; background-repeat: no-repeat;"></i>
            <?php
            }else{
               echo '<i class="fa fa-photo text-light" class="icon-block width-100 bg-grey-100"></i>';
            }
          ?>
          </span>
        </output>
        </div>
      </div>
      <div class="media-body" style="position: absolute; left: 200px; top: 175px">
      <label class="btn btn-default"><i class="fa fa-camera"></i> Change File
      <input type="file" value="Add Image" accept="image/*" id="files" name="image" class="btn btn-white btn-sm paper-shadow relative" style="display:none;" />
      </label>
      <input type="hidden" name="imagepost" value="image" />
      </div>
    </div>
</div>
<?php } ?>

<!--Video-->
<?php if($row['type'] == 'video'){ ?>
<div class="form-group">
  <label>Upload Video</label>
    <div class="media v-middle">
      <div class="media-left">
        <div class="icon-block width-100">
          <span id="span" class="icon-block width-100">
          <?php
            if(isset($row['source'])){
            ?>
            <video height=240 width=320>
              <source src="<?php echo $row['source']?>" type="video/mp4"></source>
              Your browser does not support the video tag.
            </video>
            <?php
            }else{
               echo '<i class="fa fa-film text-light" class="icon-block width-100 bg-grey-100"></i>';
            }
          ?>
          </span>
        </div>
      </div>
      <div class="media-body" style="position: absolute; top: 110px">
        <input type="file" class="btn btn-white btn-sm paper-shadow relative" id="video" name="video"/>
      </label>
      </div>
    </div>
</div>
<?php } ?>

<!--Category-->
<div class="form-group category">
  <label>Post Category</label>
  <select class="form-control" name="category">
    <option value="White_wedding">White wedding</option>
    <option value="Traditional_marriage">Traditional marriage</option>
    <option value="Accessories_designers">Accessories designers</option>
    <option value="Master_of_ceremonies">Master of ceremonies (mc)</option>
    <option value="Photographers">Photographers</option>
    <option value="Decorators">Decorators</option>
    <option value="Disc_jockey">Disc jockey (dj)</option>
    <option value="Cake_and_desert">Cake and desert</option>
    <option value="Catering_and_drinks">Catering and drinks</option>
    <option value="Bridal_couture">Bridal couture</option>
    <option value="souveniers">Souveniers</option>
  </select>
</div>
<!--Title-->
<div class="form-group title">
  <label>Post Title</label>
  <input type="text" name="title" id="title" placeholder="Title of post" class="form-control" value="<?php echo $row['title'];?>"/>
</div>
<!--Description-->
<div class="form-group description">
  <label for="description">Description</label>
  <textarea name="description" id="description" cols="30" rows="5" class="form-control" placeholder="Description of post"><?php echo $row['description'];?></textarea>
</div> 
<!---->

<script src="js/jquery.js"></script>
<script src="js/imageUpload.js"></script>