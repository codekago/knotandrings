<?php
if (!isset($_SESSION['logged_user'])) {
  header('location: signin');
}
$view_type = "video";
?>
<!--Search field-->
<div class="panel panel-default" data-toggle="panel-collapse" data-open="false">
  <div class="panel-heading panel-collapse-trigger">
    <h4 class="panel-title">Search</h4>
  </div>
  <div class="panel-body">

    <div class="dropdown">

    <div class="form-group input-group margin-none">
      <div class="row margin-none">
          <input class="form-control" autofocus type="text" placeholder="What are you looking for?" id="search" style="height: 30px;" />
        
      </div>
      <div class="input-group-btn">
        <button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
      </div>
    </div>

      <ul class="dropdown-menu col-sm-12 col-xs-12" role="menu" aria-labelldby="dLabel" id="results" style="overflow: hidden;">
      <!--load content here-->
      </ul>

    </div>
  </div>
</div>

<?php
if (isset($_GET['post'])) {

  $id = $_GET['post'];

  $q = $conn->prepare("SELECT * FROM post WHERE id = :id");
  $q->bindParam(':id', $id);
  $q->execute();

  $row = $q->fetch();

  $title = $row['title'];
  $desc = $row['description'];
  $user = $row['username'];
  $cat = $row['category'];
  $type = $row['type'];
  $id = $row['id'];
  $img = $row['source'];
  $status = $row['status'];
  $time = $row['time'];
  $date = $row['date'];
  ?>
  <!-- .panel -->
  <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
    <div class="panel-heading">
      <h4 class="panel-title">About post</h4>
    </div>
    <div class="panel-body">
      <p class="text-caption" style="font-size: 14px;">
        <i class="fa fa-clock-o fa-fw"></i> <?php echo $time; ?> &nbsp;
        <i class="fa fa-calendar fa-fw"></i> <?php echo $date; ?>
        <br/>
        <i class="fa fa-user fa-fw"></i> User: <a href="#" title="View user"><?php echo $user; ?></a>
        <br/>
        <?php
$q = $conn->prepare("SELECT * FROM rating WHERE post_id = :id");
$q->bindParam(':id', $id);

$q->execute();

$row = $q->fetch();

if ($row['post_id'] == $id) {
  $status = 1;

  $q2 = mysql_query("SELECT * FROM rating WHERE post_id = '$id'");
  $count = mysql_num_rows($q2);
  $fetch = mysql_fetch_array($q2);

  $s = mysql_query("SELECT SUM(rate) as value_sum FROM rating WHERE post_id = '$id'");
  $row_sum = mysql_fetch_assoc($s);
  $sum1 = $row_sum['value_sum'];

  $sum2 = round($sum1/$count, 1);

  $s = mysql_query("SELECT SUM(rate) as value_sum FROM rating WHERE post_id = '$id'");
  $row_sum = mysql_fetch_assoc($s);
  $sum1 = $row_sum['value_sum'];

  @$sum = round($sum1/$count);
  //echo $sum;
}else{
  $status = 0;
}
?>
<i class="fa fa-check fa-fw"></i> Review: <?php
if ($status == 0) {
  echo "<b>0</b>";
}else{
echo "<b>$count</b>";
}

?> <i class="fa fa-user"></i>
<br/>
<i class="fa fa-star fa-fw"></i> Rate: <?php
if ($status == 0) {
  echo "0 ";
}else{
  echo "<b>$sum2 </b>";
}

if (!isset($sum)) {
  $sum = 0;
}


if ($sum == 1) {
  ?>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($sum == 2) {
    ?>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($sum == 3) {
    ?>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($sum == 4) {
    ?>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($sum == 5) {
    ?>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
  <?php
}else{
  ?>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}
?>
</p>
</div>
<hr class="margin-none" />
<div class="panel-body text-center">

            <?php
            $user = $_SESSION['logged_user'];
$check_user = $conn->prepare("SELECT * FROM rating WHERE post_id = :id && username = :user");
$check_user->bindParam(':id', $id);
$check_user->bindParam(':user', $user);

$check_user->execute();
$user_check = $check_user->rowCount();

if ($user_check > 0) {
  $row = $check_user->fetch();
  $rating = $row['rate'];
?>
  <b>Your rating</b><br>
<?php
  if ($rating == 1) {
  ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rating == 2) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rating == 3) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rating == 4) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rating == 5) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
  <?php
}else{
  ?>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}
}else{
  ?>
<p><a class="btn btn-success btn-lg paper-shadow relative" data-z="1" data-hover-z="2" data-animated  href="#reviewModal" data-toggle="modal">Rate post</a></p>
  <?php
}
            ?>
              
            </div>
            <ul class="list-group">
              <li class="list-group-item">
                <a href="#" class="text-light"><i class="fa fa-facebook fa-fw"></i> Share on facebook</a>
              </li>
              <li class="list-group-item">
                <a href="#" class="text-light"><i class="fa fa-twitter fa-fw"></i> Tweet this post</a>
              </li>
            </ul>
          </div>
          <!-- // END .panel -->

          <?php
}
          ?>

<div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
  <div class="panel-heading panel-collapse-trigger">
  <h4 class="panel-title">Category</h4>
  </div>
  <div class="panel-body list-group">
    <ul class="list-group">
      <?php
        $List = array('all','white_wedding','traditional_marriage','accessories_designers','master_of_ceremonies',
          'photographers','decorators','dick_jockey','cake_and_drinks','catering_and_drinks','bridal_couture',
          'souveniers');

        for($a = 0; $a < count($List); $a++){
            echo "<li class='list-group-item ";
            
            if(!isset($_GET['category']) && $page == 'video' && $a == 0){ 
              echo 'active'; 
            }elseif(!isset($_GET['category']) && $a == 0) {
              echo 'active';
            }elseif(isset($_GET['category']) && $_GET['category'] == $List[$a]){
              echo 'active';
            }

            echo "'><span class='badge pull-right'>";

            if($a == 0){
              $s = $conn->prepare("SELECT * FROM post WHERE type = :type");
            }else{
              $s = $conn->prepare("SELECT * FROM post WHERE category = :category AND type = :type");
              $s->bindParam(':category', $List[$a]);
            }
            $s->bindParam(':type', $view_type);
            $s->execute();
            $count = $s->rowCount();
            echo $count;


            $links = explode("_", $List[$a]);
            $link = ucfirst(implode(" ", $links));

            echo "</span> <a class='list-group-link' href='?category=".$List[$a]."'>".$link."</a>";
            echo "</li>";
        }
      ?>
    </ul>
  </div>
</div>

<!--Section for advrts-->
<h4>Featured</h4>
<div class="slick-basic slick-slider" data-items="1" data-items-lg="1" data-items-md="1" data-items-sm="1" data-items-xs="1">
  <div class="item">
    <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
      <div class="panel-body">
        <div class="media media-clearfix-xs">
          <div class="media-left">
            <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
              <span class="img icon-block s90 bg-default"></span>
              <span class="overlay overlay-full padding-none icon-block s90 bg-default">
              <span class="v-center">
                  <i class="fa fa-github"></i>
              </span>
              </span>
              <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                <span class="v-center">
                  <span class="btn btn-circle btn-white btn-lg"><i class="fa fa-graduation-cap"></i></span>
                </span>
              </a>
            </div>
          </div>
          <div class="media-body">
            <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Github Webhooks for Beginners</a></h4>
            <p class="small margin-none">
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star-o text-yellow-800"></span>
              <span class="fa fa-fw fa-star-o text-yellow-800"></span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="item">
    <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
      <div class="panel-body">
        <div class="media media-clearfix-xs">
          <div class="media-left">
            <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
              <span class="img icon-block s90 bg-primary"></span>
              <span class="overlay overlay-full padding-none icon-block s90 bg-primary">
              <span class="v-center">
                  <i class="fa fa-css3"></i>
              </span>
              </span>
              <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                <span class="v-center">
                  <span class="btn btn-circle btn-primary btn-lg"><i class="fa fa-graduation-cap"></i></span>
                </span>
              </a>
            </div>
          </div>
          <div class="media-body">
            <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Awesome CSS with LESS Processing</a></h4>
            <p class="small margin-none">
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star-o text-yellow-800"></span>
              <span class="fa fa-fw fa-star-o text-yellow-800"></span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="item">
    <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
      <div class="panel-body">
        <div class="media media-clearfix-xs">
          <div class="media-left">
            <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
              <span class="img icon-block s90 bg-lightred"></span>
              <span class="overlay overlay-full padding-none icon-block s90 bg-lightred">
              <span class="v-center">
                  <i class="fa fa-windows"></i>
              </span>
              </span>
              <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                <span class="v-center">
                  <span class="btn btn-circle btn-red-500 btn-lg"><i class="fa fa-graduation-cap"></i></span>
                </span>
              </a>
            </div>
          </div>
          <div class="media-body">
            <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Portable Environments with Vagrant</a></h4>
            <p class="small margin-none">
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star-o text-yellow-800"></span>
              <span class="fa fa-fw fa-star-o text-yellow-800"></span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="item">
    <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
      <div class="panel-body">
        <div class="media media-clearfix-xs">
          <div class="media-left">
            <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
              <span class="img icon-block s90 bg-brown"></span>
              <span class="overlay overlay-full padding-none icon-block s90 bg-brown">
              <span class="v-center">
                  <i class="fa fa-wordpress"></i>
              </span>
              </span>
              <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                <span class="v-center">
                  <span class="btn btn-circle btn-orange-500 btn-lg"><i class="fa fa-graduation-cap"></i></span>
                </span>
              </a>
            </div>
          </div>
          <div class="media-body">
            <h4 class="media-heading margin-v-5-3"><a href="website-course.html">WordPress Theme Development</a></h4>
            <p class="small margin-none">
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star-o text-yellow-800"></span>
              <span class="fa fa-fw fa-star-o text-yellow-800"></span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="item">
    <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
      <div class="panel-body">
        <div class="media media-clearfix-xs">
          <div class="media-left">
            <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
              <span class="img icon-block s90 bg-purple"></span>
              <span class="overlay overlay-full padding-none icon-block s90 bg-purple">
              <span class="v-center">
                  <i class="fa fa-jsfiddle"></i>
              </span>
              </span>
              <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                <span class="v-center">
                  <span class="btn btn-circle btn-purple-500 btn-lg"><i class="fa fa-graduation-cap"></i></span>
                </span>
              </a>
            </div>
          </div>
          <div class="media-body">
            <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Modular JavaScript with Browserify</a></h4>
            <p class="small margin-none">
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star-o text-yellow-800"></span>
              <span class="fa fa-fw fa-star-o text-yellow-800"></span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="item">
    <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
      <div class="panel-body">
        <div class="media media-clearfix-xs">
          <div class="media-left">
            <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
              <span class="img icon-block s90 bg-default"></span>
              <span class="overlay overlay-full padding-none icon-block s90 bg-default">
              <span class="v-center">
                  <i class="fa fa-cc-visa"></i>
              </span>
              </span>
              <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                <span class="v-center">
                  <span class="btn btn-circle btn-white btn-lg"><i class="fa fa-graduation-cap"></i></span>
                </span>
              </a>
            </div>
          </div>
          <div class="media-body">
            <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Easy Online Payments with Stripe</a></h4>
            <p class="small margin-none">
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star text-yellow-800"></span>
              <span class="fa fa-fw fa-star-o text-yellow-800"></span>
              <span class="fa fa-fw fa-star-o text-yellow-800"></span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>