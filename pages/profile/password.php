<!-- Tabbable Widget -->
          <div class="tabbable paper-shadow relative" data-z="0.5">

            <!-- Tabs -->
            <ul class="nav nav-tabs">
              <li <?php
if (isset($_GET['ref'])) {

      if ($_GET['ref'] == 'view') {
      echo 'class="active"';
    }

}else{
  echo 'class="active"';
}
              ?>><a href="profile?ref=view" title="View Profile" style="cursor: pointer;"><i class="fa fa-fw fa-eye"></i> <span class="hidden-sm hidden-xs">View Profile</span></a></li>
              <li <?php
              if (isset($_GET['ref'])) {
  
      if ($_GET['ref'] == 'edit') {
        echo 'class="active"';
      }

}
              ?>><a href="profile?ref=edit" title="Edit Profile" style="cursor: pointer;"><i class="fa fa-fw fa-pencil-square-o"></i> <span class="hidden-sm hidden-xs">Edit Profile</span></a></li>
              <li <?php
if (isset($_GET['ref'])) {

      if ($_GET['ref'] == 'password') {
      echo 'class="active"';
    }

}else{
  echo 'class="active"';
}
              ?>><a href="profile?ref=password" title="Change Password" style="cursor: pointer;"><i class="fa fa-fw fa-lock"></i> <span class="hidden-sm hidden-xs">Change Password</span></a></li>
            </ul>
            <!-- // END Tabs -->

            <!-- Panes -->
            <div class="tab-content">

              <div id="account" class="tab-pane active">
              <?php
if (isset($_POST['old_pwd'])) {
  $old = securePwd($_POST['old_pwd']);
  $new = securePwd($_POST['new_pwd']);

  $q = $conn->prepare("SELECT * FROM account WHERE password = :old");
  $q->bindParam(':old', $old);

  $q->execute();

  $row = $q->fetch();

  if ($row['password'] == $old) {
    $update = $conn->prepare("UPDATE account SET password = :pwd WHERE username = :user");
    $update->bindParam(':pwd', $new);
    $update->bindParam(':user', $user);

    if ($update->execute()) {
      ?>
<div class="alert alert-success">
<strong>Your password have been successfully changed.</strong>
</div>
      <?php
    }else{
?>
<div class="alert alert-danger">
<strong>Your password change was not successful.</strong>
</div>
<?php
    }//updating the user password


  }else{
?>
<div class="alert alert-danger">
<strong>Your old password is incorrect.</strong>
</div>
<?php
  }//password check


}else{

}
              ?>
                <form class="form-horizontal" action="profile?ref=password" method="post">
                  <div class="form-group">
                    <label for="inputPassword3" class="col-md-2 control-label">Old Password</label>
                    <div class="col-md-6">
                      <div class="form-control-material">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                          <input type="password" autofocus class="form-control" name="old_pwd" id="inputPassword3" placeholder="Old Password" required>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-md-2 control-label">New Password</label>
                    <div class="col-md-6">
                      <div class="form-control-material">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                          <input type="password" class="form-control" name="new_pwd" id="inputPassword3" placeholder="New Password" required>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group margin-none">
                    <div class="col-md-offset-2 col-md-10">
                      <button type="submit" class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Change Password</button>
                    </div>
                  </div>
                </form>
              </div>

            </div>
            <!-- // END Panes -->

          </div>
          <!-- // END Tabbable Widget -->