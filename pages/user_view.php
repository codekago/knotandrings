  <?php
if (isset($_GET['username'])) {
  
  $user  = secureTxt($_GET['username']);
  $logged_user = $_SESSION['logged_user'];

  $q = $conn->prepare("SELECT * FROM profile WHERE username = :user");
  $q->bindParam(':user', $user);

  $q->execute();

if ($q->rowCount() != 0) {
  $status = 1;
  $row = $q->fetch();
  $_SESSION['user_profile'] = $user;
  $name = $row['name'];
  $email = $row['email'];
  $phone = $row['phone'];
  $privacy = $row['privacy'];
  $image = $row['image'];
  $gender = $row['gender'];
  $website = $row['website'];
}else{
  $status = 0;
}

}else{
  $status = 00;
}
  ?>
  <div class="container">

    <div class="page-section">
      <div class="row">

        <div class="col-md-3">
        <?php
if ($status == 00 || $status == 0) {
  require('pages/explore_sidemenu.php');
}elseif ($status == 1) {
  require('pages/user/sidemenu.php');
}
        ?>
</div>

        <div class="col-md-9">

<?php
if ($status == 00) {
  $category = 'all';
  require('pages/category.php');
}elseif ($status == 1) {
  require('pages/user/view.php');
}elseif ($status == 0) {
  ?>
<div class="alert alert-danger">
<strong>User does not exist in knot&amp;rings</strong>
</div>
  <?php
}
        ?>

        </div>

      </div>
    </div>

  </div>
