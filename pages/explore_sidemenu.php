<?php
if (!isset($_SESSION['logged_user'])) {
  header('location: signin');
}
?>
          <!--Search field-->
<div class="panel panel-default" data-toggle="panel-collapse" data-open="false">
  <div class="panel-heading panel-collapse-trigger">
    <h4 class="panel-title">Search</h4>
  </div>
  <div class="panel-body">

    <div class="dropdown">

    <div class="form-group input-group margin-none">
      <div class="row margin-none">
          <input class="form-control" autofocus type="text" placeholder="What are you looking for?" id="search" style="height: 30px;" />
        
      </div>
      <div class="input-group-btn">
        <button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
      </div>
    </div>

      <ul class="dropdown-menu col-sm-12 col-xs-12" role="menu" aria-labelldby="dLabel" id="results" style="overflow: hidden;">
      <!--load content here-->
      </ul>

    </div>
  </div>
</div>

          <?php
if (isset($_GET['post'])) {

  $id = $_GET['post'];

  $q = $conn->prepare("SELECT * FROM post WHERE id = :id");
  $q->bindParam(':id', $id);
  $q->execute();

  $row = $q->fetch();

  $title = $row['title'];
  $desc = $row['description'];
  $user = $row['username'];
  $cat = $row['category'];
  $type = $row['type'];
  $id = $row['id'];
  $img = $row['source'];
  $status = $row['status'];
  $time = $row['time'];
  $date = $row['date'];
  ?>
  <!-- .panel -->
          <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
            <div class="panel-heading">
              <h4 class="panel-title">About post</h4>
            </div>
            <div class="panel-body">
              <p class="text-caption" style="font-size: 14px;">
                <i class="fa fa-clock-o fa-fw"></i> <?php echo $time; ?> &nbsp;
                <i class="fa fa-calendar fa-fw"></i> <?php echo $date; ?>
                <br/>
                <i class="fa fa-user fa-fw"></i> User: <?php
if ($user == 'knot&rings') {
   ?>
 <a href="javascript:;" title="knot&amp;rings"><?php echo $user; ?></a>
  <?php
}else{
  ?>
 <a href="user_view?username=<?php echo $user; ?>" title="View user"><?php echo $user; ?></a>
  <?php
}
                ?>
                <br/>
                <?php
$q = $conn->prepare("SELECT * FROM rating WHERE post_id = :id");
$q->bindParam(':id', $id);

$q->execute();

$row = $q->fetch();

if ($row['post_id'] == $id) {
  $status = 1;

  $q2 = mysql_query("SELECT * FROM rating WHERE post_id = '$id'");
$count = mysql_num_rows($q2);
$fetch = mysql_fetch_array($q2);

$s = mysql_query("SELECT SUM(rate) as value_sum FROM rating WHERE post_id = '$id'");
$row_sum = mysql_fetch_assoc($s);
$sum1 = $row_sum['value_sum'];

$sum2 = round($sum1/$count, 1);

$s = mysql_query("SELECT SUM(rate) as value_sum FROM rating WHERE post_id = '$id'");
$row_sum = mysql_fetch_assoc($s);
$sum1 = $row_sum['value_sum'];

@$sum = round($sum1/$count);
//echo $sum;


}else{
  $status = 0;
}
                ?>
                <i class="fa fa-check fa-fw"></i> Review: <?php
if ($status == 0) {
  echo "<b>0</b>";
}else{
echo "<b>$count</b>";
}

                ?> <i class="fa fa-user"></i>
                <br/>
                <i class="fa fa-star fa-fw"></i> Rate: <?php
if ($status == 0) {
  echo "0 ";
}else{
  echo "<b>$sum2 </b>";
}

if (!isset($sum)) {
  $sum = 0;
}


if ($sum == 1) {
  ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($sum == 2) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($sum == 3) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($sum == 4) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($sum == 5) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
  <?php
}else{
  ?>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}
                ?>
              </p>
            </div>
            <hr class="margin-none" />
            <div class="panel-body text-center">

            <?php
            $user = $_SESSION['logged_user'];
$check_user = $conn->prepare("SELECT * FROM rating WHERE post_id = :id && username = :user");
$check_user->bindParam(':id', $id);
$check_user->bindParam(':user', $user);

$check_user->execute();
$user_check = $check_user->rowCount();

if ($user_check > 0) {
  $row = $check_user->fetch();
  $rating = $row['rate'];
?>
  <b>Your rating</b><br>
<?php
  if ($rating == 1) {
  ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rating == 2) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rating == 3) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rating == 4) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rating == 5) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
  <?php
}else{
  ?>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}
}else{
  ?>
<p><a class="btn btn-success btn-lg paper-shadow relative" data-z="1" data-hover-z="2" data-animated  href="#reviewModal" data-toggle="modal">Rate post</a></p>
  <?php
}
            ?>
              
            </div>
            <ul class="list-group">
              <li class="list-group-item">
                <center>
                <h4 class="panel-title">Share post</h4><br>
<span class='st_facebook_large' displayText='Facebook'></span>
<span class='st_twitter_large' displayText='Tweet'></span>
<span class='st_googleplus_large' displayText='Google +'></span>
<span class='st_instagram_large' displayText='Instagram Badge' st_username='knotandrings'></span>
<span class='st_pinterest_large' displayText='Pinterest'></span>
                </center>
              </li>
            </ul>
          </div>
          <!-- // END .panel -->

          <?php
}
          ?>

          <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
            <div class="panel-heading panel-collapse-trigger">
              <h4 class="panel-title">Category</h4>
            </div>
            <div class="panel-body list-group">
              <ul class="list-group">
                <li class="list-group-item <?php
                if (isset($_GET['post'])) {
                  
                }elseif ($_GET['category'] == 'all') {
  echo 'active';
}elseif(!isset($_GET['category']) && $page == 'explore'){
echo 'active';
}
                ?>">
                  <span class="badge pull-right"><?php
$s = $conn->prepare("SELECT * FROM post");
$s->execute();
$count = $s->rowCount();
echo $count;
                    ?></span>
                  <a class="list-group-link" href="?category=all">All</a>
                </li>
                <li class="list-group-item <?php
if ($_GET['category'] == 'white_wedding') {
  echo 'active';
}
                ?>">
                  <span class="badge pull-right">
                    <?php
$s = $conn->prepare("SELECT * FROM post WHERE category = 'white_wedding'");
$s->execute();
$count = $s->rowCount();
echo $count;
                    ?>
                  </span>
                  <a class="list-group-link" href="?category=white_wedding">White wedding</a>
                </li>
                <li class="list-group-item <?php
if ($_GET['category'] == 'traditional_marriage') {
  echo 'active';
}
                ?>">
                  <span class="badge pull-right"><?php
$s = $conn->prepare("SELECT * FROM post WHERE category = 'traditional_marriage'");
$s->execute();
$count = $s->rowCount();
echo $count;
                    ?></span>
                  <a class="list-group-link" href="?category=traditional_marriage">Traditional marriage</a>
                </li>
                <li class="list-group-item <?php
if ($_GET['category'] == 'accessories_designers') {
  echo 'active';
}
                ?>">
                  <span class="badge pull-right"><?php
$s = $conn->prepare("SELECT * FROM post WHERE category = 'accessories_designers'");
$s->execute();
$count = $s->rowCount();
echo $count;
                    ?></span>
                  <a class="list-group-link" href="?category=accessories_designers">Accessories designers</a>
                </li>
                <li class="list-group-item <?php
if ($_GET['category'] == 'master_of_ceremonies') {
  echo 'active';
}
                ?>">
                  <span class="badge pull-right"><?php
$s = $conn->prepare("SELECT * FROM post WHERE category = 'master_of_ceremonies'");
$s->execute();
$count = $s->rowCount();
echo $count;
                    ?></span>
                  <a class="list-group-link" href="?category=master_of_ceremonies">Master of ceremonies (mc)</a>
                </li>
                <li class="list-group-item <?php
if ($_GET['category'] == 'photographers') {
  echo 'active';
}
                ?>">
                  <span class="badge pull-right"><?php
$s = $conn->prepare("SELECT * FROM post WHERE category = 'photographers'");
$s->execute();
$count = $s->rowCount();
echo $count;
                    ?></span>
                  <a class="list-group-link" href="?category=photographers">Photographers</a>
                </li>
                <li class="list-group-item <?php
if ($_GET['category'] == 'decorators') {
  echo 'active';
}
                ?>">
                  <span class="badge pull-right"><?php
$s = $conn->prepare("SELECT * FROM post WHERE category = 'decorators'");
$s->execute();
$count = $s->rowCount();
echo $count;
                    ?></span>
                  <a class="list-group-link" href="?category=decorators">Decorators</a>
                </li>
                <li class="list-group-item <?php
if ($_GET['category'] == 'dick_jockey') {
  echo 'active';
}
                ?>">
                  <span class="badge pull-right"><?php
$s = $conn->prepare("SELECT * FROM post WHERE category = 'dick_jockey'");
$s->execute();
$count = $s->rowCount();
echo $count;
                    ?></span>
                  <a class="list-group-link" href="?category=dick_jockey">Disc jockey (dj)</a>
                </li>
                <li class="list-group-item <?php
if ($_GET['category'] == 'cake_and_drinks') {
  echo 'active';
}
                ?>">
                  <span class="badge pull-right"><?php
$s = $conn->prepare("SELECT * FROM post WHERE category = 'cake_and_drinks'");
$s->execute();
$count = $s->rowCount();
echo $count;
                    ?></span>
                  <a class="list-group-link" href="?category=cake_and_drinks">Cake and desert</a>
                </li>
                <li class="list-group-item <?php
if ($_GET['category'] == 'catering_and_drinks') {
  echo 'active';
}
                ?>">
                  <span class="badge pull-right"><?php
$s = $conn->prepare("SELECT * FROM post WHERE category = 'catering_and_drinks'");
$s->execute();
$count = $s->rowCount();
echo $count;
                    ?></span>
                  <a class="list-group-link" href="?category=catering_and_drinks">Catering and drinks</a>
                </li>
                <li class="list-group-item <?php
if ($_GET['category'] == 'bridal_couture') {
  echo 'active';
}
                ?>">
                  <span class="badge pull-right"><?php
$s = $conn->prepare("SELECT * FROM post WHERE category = 'bridal_couture'");
$s->execute();
$count = $s->rowCount();
echo $count;
                    ?></span>
                  <a class="list-group-link" href="?category=bridal_couture">Bridal couture</a>
                </li>
                <li class="list-group-item <?php
if ($_GET['category'] == 'souveniers') {
  echo 'active';
}
                ?>">
                  <span class="badge pull-right"><?php
$s = $conn->prepare("SELECT * FROM post WHERE category = 'souveniers'");
$s->execute();
$count = $s->rowCount();
echo $count;
                    ?></span>
                  <a class="list-group-link" href="?category=souveniers">Souveniers</a>
                </li>
              </ul>
            </div>
          </div>

