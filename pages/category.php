	<?php
if($category == '404'){
?>
<div class="row grid js-masonry" data-masonry-options='{ "itemSelector": ".grid-item", "columWidth": 200 }' data-toggle="isotope">
  <div class="item col-xs-12 col-sm-12 col-lg-12 grid-item" style="text-align: center;">
    <i class="fa fa-warning fa-5x"></i><br>
    Oops! Sorry the category was not found!
  </div>
</div>
<?php
}

if ($category == 'all') {
  if(isset($view_type) OR @$view_type == 'video'){
   $q = $conn->prepare("SELECT * FROM post WHERE type = :type ORDER BY id DESC");
   $q->bindParam(':type', $view_type);
  }else{
   $q = $conn->prepare("SELECT * FROM post ORDER BY id DESC");
  }
}else{
  if(isset($view_type) OR @$view_type == 'video'){
   $q = $conn->prepare("SELECT * FROM post WHERE category = :category AND type = :type ORDER BY id DESC");  
   $q->bindParam(':type', $view_type);  
  }else{
   $q = $conn->prepare("SELECT * FROM post WHERE category = :category ORDER BY id DESC");
  }
  $q->bindParam(':category', $category);
}
?>
<div class="row grid js-masonry"
  data-masonry-options='{ "itemSelector": ".grid-item", "columWidth": 200 }' data-toggle="isotope">
  <?php

$q->execute();

function limit_words($string, $word_limit)
{
    $words = explode(" ",$string);
    return implode(" ",array_splice($words,0,$word_limit));
}

while ($row = $q->fetch()) {
  $post_views = $row['views'];
  ?>
<div class="item col-xs-12 col-sm-6 col-lg-6 grid-item">
              <div class="panel panel-default paper-shadow" data-z="0.5">

               <a href="<?php if(isset($view_type) && $view_type == 'video'){ echo 'video'; }else{ echo 'explore'; }?>?post=<?php echo $row['id']; ?>" id="<?php echo $row['id']; ?>" class="view-post">
    <?php if($row['type'] == 'image'){ ?>
    <img src="<?php echo $row['source']; ?>" class="cover overlay cover-image-full hover" style="width: 100%; max-height: 400px">
    <?php }else{ ?>
    <div class="embed-responsive embed-responsive-16by9">
      <i class="fa fa-play fa-3x video_button" style="visibility: hidden;" id='<?php echo "btn_".$row['id']; ?>' onmouseenter="show_btn(<?php echo $row['id']; ?>);" onmouseout="hide_btn(<?php echo $row['id']; ?>);"></i>
      <video class="embed-responsive-item" onmouseenter="show_btn(<?php echo $row['id']; ?>);" onmouseout="hide_btn(<?php echo $row['id']; ?>);">
        <source src="<?php echo $row['source']?>" type="video/mp4"></source>
        Your browser does not support the video tag.
      </video>
    </div>
    <?php } ?>
    </a>

                <div class="panel-body" style="padding-top: 0px;">
                  <h4 class="text-headline margin-v-0-10"><a href="explore?post=<?php echo $row['id']; ?>" id="<?php echo $row['id']; ?>" class="view-post"><?php echo $row['title']; ?></a></h4>

                  <p class="small margin-none">
                  <?php
                  $id = $row['id'];
//echo $id;
  $q3 = $conn->prepare("SELECT rate FROM rating WHERE post_id = :id");
  $q3->bindParam(':id', $id);

  $q3->execute();
  $count = $q3->rowCount();


  if ($count > 0) {
    
      /*$array[] = $row3['rate'];
      $r2 = array_sum($array);
      echo $r2;*/

      $sum = 0;
      $ids_array = array();
      while ($row3 = $q3->fetch()) {
        
   $sum+= $row3['rate'];

      }

      
$rate = round($sum/$count);
$rate2 = round($sum/$count, 1);

?>
<b>Reviews: <?php echo $count; ?>  &nbsp;
Rate: <?php echo $rate2; ?>
</b>
<?php

if ($rate == 1) {
  ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rate == 2) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rate == 3) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rate == 4) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rate == 5) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
  <?php
}else{
  ?>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}
    
  }else{
    ?>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
  }

  

?>
                  </p>

                </div>
                <hr class="margin-none" />
                <div class="panel-body">
                  <p>
                  <?php 
                  $content =  $row['description'];

                  if (str_word_count($content) > 35) {
                    echo limit_words($content,35)."...";
                  }else{
                    echo $content;
                  }
                  
                  ?>
                  </p>

                  <div class="media v-middle">
                    <div class="media-left">
                      <img src="<?php
                      $q1 = $conn->prepare("SELECT * FROM profile WHERE username = :user");
$q1->bindParam(':user', $row['username']);
$q1->execute();

while ($row2 = $q1->fetch()) {
  echo $row2['image'];
}

                      ?>" alt="profile image" class="img-circle width-40" />
                    </div>
                    <div class="media-body">
                      <h4 style="margin-bottom: 0px;"><a href="user_view?username=<?php echo $row['username']; ?>"><?php echo $row['username']; ?></a>
                        <br/>
                      </h4>
                      <b><?php echo timeAgo($row['timestamp']); ?></b>
                      <p class="pull-right"><b>
<?php
if ($post_views == '') {
  echo "0 view";
}elseif ($post_views == 0 || $post_views == 1) {
   echo number_format($post_views)." view";
 }else{
echo number_format($post_views)." views";
 }
?>
                      </b></p>
                    </div>
                  </div>

                </div>

              </div>
            </div>
  <?php
}
  ?>
            
            
          </div>

       