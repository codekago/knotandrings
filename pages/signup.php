<div id="body" style="height: 100%;">
    <div class="container">
        <header>
            <h1 class="logo"><a href="signup"><img src="img/logo.png" height="70px" /></a></h1>
            <div class="nav" style="float: right;">
                <p class="ask">Already using KnotandRings?</p>
                <a href="signin" class="btn btn-default signup" style="padding: 15px 15px;">Sign In</a>
            </div>
        </header>
        
       
        <h2 class="home_text_head">LETS RATE YOU</h2>

        <section class="">
            <p class="home_text_body">Knot and Rings is the world's first online wedding rating magazine, focused on showcasing iconic wedding moments.</p>
            <ul class="social">
                <li class="social_item"><a href="#" class="fb homefb"><span class="socicon socicon-facebook"></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sign Up with Facebook</a></li>
            </ul>
            
            <?php
if (isset($_POST['email'])) {
    $user = secureTxt($_POST['user']);
    $email = secureTxt($_POST['email']);
    $pwd = securePwd($_POST['pwd']);

    $code = rand('1642853729', '9356782341');

    $verification_code = securePwd($code);

    $q = $conn->prepare("SELECT * FROM account WHERE username = :user");
    $q->bindParam(':user', $user);

    $q->execute();

    $row = $q->fetch();

    if($row['username'] == $user) {
?>
<div class="alert alert-warning">
<strong>This username is already registered</strong>
</div>
<?php
    }else{
$insert = $conn->prepare("INSERT INTO account (username, email, password, signup_date, signup_time, verification_code) VALUES (:user, :email, :pwd, :signup_date, :signup_time, :code)");
$insert->bindParam(':user', $user);
$insert->bindParam(':email', $email);
$insert->bindParam(':pwd', $pwd);
$insert->bindParam(':code', $verification_code);
$insert->bindParam(':signup_time', $t);
$insert->bindParam(':signup_date', $d);

$insert2 = $conn->prepare("INSERT INTO profile (email, username) VALUES (:email, :user)");
$insert2->bindParam(':email', $email);
$insert2->bindParam(':user', $user);

if ($insert->execute() && $insert2->execute()) {
   ?>
<div class="alert alert-success">
<strong>Your registration was successful.</strong>
<br>A verification link have been sent to your inbox</div>
   <?php
}else{
?>
<div class="alert alert-danger">
<strong>Your registration was not successful.</strong>
<br>Please try again.
</div>
<?php
}//inserting into database

    }//good pass


}else{
    echo '<p class="small">or via email</p>';
}
            ?>


            <form role="form" action="signup" method="post">

              <div class="form-group">

                <input type="text" name="user" id="user" required class="form-control" autofocus placeholder="Your Username">
                <input type="email" name="email" required class="form-control" id="email" placeholder="Your Email Address">
                <input type="password" name="pwd" required class="form-control" id="pwd" placeholder="Your Password">
              </div>
              
              <button type="submit" class="btn btn-warning">Sign Up</button>
            </form>

            <p class="forgot">By signing up, you agree to KnotandRings <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.</p>
        </section>

        
        <ul class="bottom_links">
            <li><a href="about">About Us</a></li>
            <li><a href="explore">Explore</a></li>
        </ul>

    </div>

    </div>