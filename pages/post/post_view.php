<?php
if (isset($_GET['id'])) {
  $id = $_GET['id'];

  $q = $conn->prepare("SELECT * FROM post WHERE id = :id");
  $q->bindParam(':id', $id);
  $q->execute();

  $row = $q->fetch();

  $title = $row['title'];
  $desc = $row['description'];
  $user = $row['username'];
  $cat = $row['category'];
  $type = $row['type'];
  $img = $row['source'];
  $status = $row['status'];

  ?>
<div class="page-section">
          <div class="width-300-md width-100pc-xs paragraph-inline" style="float: none; width: 100%;">
            <div class="embed-responsive embed-responsive-16by9">
              <img class="embed-responsive-item" src="../<?php echo $img; ?>">
            </div>
          </div>
          <p><?php echo $desc; ?></p>
          <br/>
          <p class="margin-none">
            <span class="label bg-gray-dark"><?php echo $cat; ?></span>
            <span>
                <a class="btn btn-white btn-circle paper-shadow relative" title="report post" data-z="1" href="#"><i class="glyphicon glyphicon-eye-close"></i></a>
              </span>
              <div style="clear: both"></div>
          </p>
        </div>

        <div class="page-section">
          <div class="row">

              <!-- <div class="pull-right">
                <a class="btn btn-white btn-circle paper-shadow relative" data-z="1" href="#"><i class="glyphicon glyphicon-thumbs-down"></i></a>
              </div> -->

              <h2 class="text-headline margin-none">Reviews</h2>
              <p class="text-subhead text-light">What people say about this post</p>
              <div class="slick-basic slick-slider" data-items="1" data-items-lg="1" data-items-md="1" data-items-sm="1" data-items-xs="1">
              <?php
if (isset($_GET['ref']) && $_GET['ref'] == 'post_view') {
  
if (isset($_GET['id'])) {
  $id = $_GET['id'];

  $q = $conn->prepare("SELECT * FROM rating WHERE post_id = :id ORDER BY id DESC");
  $q->bindParam(':id', $id);

  $q->execute();

  while ($row = $q->fetch()) {
    ?>
<div class="item">
                  <div class="testimonial">
                    <div class="panel panel-default">
                      <div class="panel-body">
                        <p><?php echo $row['review']; ?></p>
                      </div>
                    </div>
                    <div class="media v-middle">
                      <div class="media-left">
                      <?php
if ($row['role'] == 'user') {
  $table = 'profile';

}else{
  $table = 'staff';
}

$q1 = $conn->prepare("SELECT * FROM $table WHERE username = :user");
$q1->bindParam(':user', $row['username']);
$q1->execute();

while ($row2 = $q1->fetch()) {
  ?>
<img src="<?php 
if ($table == 'profile') {
  echo "../".$row2['image'];
}else{
  echo $row2['photo'];
}
 ?>" alt="User image" class="img-circle width-40" />
  <?php
}
                      ?>
                        
                      </div>
                      <div class="media-body">
                        <p class="text-subhead margin-v-5-0">
                          <strong><a href="#"><?php echo $row['username']; ?><span class="text-muted"></span></a></strong>
                        </p>
                        <p class="small">
                          <?php
if ($row['rate'] == 1) {
  ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($row['rate'] == 2) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($row['rate'] == 3) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($row['rate'] == 4) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($row['rate'] == 5) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
  <?php
}
                          ?>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
    <?php
  }
}

}
?>
                
              </div>

            
            
          </div>

        </div>
  <?php
}else{
  
}
