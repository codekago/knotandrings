<!-- Tabbable Widget -->
          <div class="tabbable paper-shadow relative" data-z="0.5">

<!-- Tabs -->
<ul class="nav nav-tabs">
  <li <?php if (isset($_GET['type']) && @$_GET['type'] == 'image') echo "class='active'"; ?>>
    <a href="post?ref=add&type=image" style="cursor: pointer;">
      <i class="fa fa-fw fa-image"></i> <span class="hidden-sm hidden-xs">Add Image Post</span>
    </a>
  </li>
  <li <?php if(isset($_GET['type']) && @$_GET['type'] == 'video') echo "class='active'";?>>
    <a href="post?ref=add&type=video" style="cursor: pointer;">
      <i class="fa fa-fw fa-film"></i> <span class="hidden-sm hidden-xs">Add Video Post</span>
    </a>
  </li>
</ul>
<!-- // END Tabs -->

<!-- Panes -->
<div class="tab-content">

  <div id="course" class="tab-pane active">
<?php
/*Image ypload*/
if (isset($_POST['title'])) {

  $title = secureTxt($_POST['title']);
  $description = secureTxt($_POST['description']);
  $category = secureTxt($_POST['category']);
  $type = "image";

  $target_dir = "uploads/post/";
  $target_file = $target_dir . basename($_FILES["image"]["name"]);
  $uploadOk = 1;
  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

  //convert file type extension to lower case
  $imageFileType = strtolower($imageFileType);

  // Check if image file is a actual image or fake image
  if(isset($_POST["name"])) {
      $check = getimagesize($_FILES["image"]["tmp_name"]);
      if($check !== false) {
          
          $uploadOk = 1;
      } else {
          echo "<div class='alert alert-warning'>File is not an image.</div>";
          $uploadOk = 0;
      }
  }
  
  // Check if file already exists
  if (file_exists($target_file)) {
    echo "<div class='alert alert-warning'>Sorry, photo already exists.</div>";
    $uploadOk = 0;
  }

  // Check file size
  if ($_FILES["image"]["size"] > 5000000) {
      echo "<div class='alert alert-warning'>Sorry, your photo is too large.</div>";
      $uploadOk = 0;
  }
  // Allow certain file formats
  if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
  && $imageFileType != "gif" ) {
      echo "<div class='alert alert-warning'>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</div>";
      $uploadOk = 0;
  }
  // Check if $uploadOk is set to 0 by an error
  if ($uploadOk == 0) {
      echo "<div class='alert alert-danger'>Sorry, your photo can not be uploaded.</div>";
  // if everything is ok, try to upload file
  } else {
  $q = $conn->prepare("INSERT INTO post (username, title, description, type, category, source, date, time, timestamp, views) VALUES (:user, :title, :description, :type, :category, :source, :d, :t, :stamp, '0')");

  $q->bindParam(':category', $category);
  $q->bindParam(':title', $title);
  $q->bindParam(':type', $type);
  $q->bindParam(':d', $d);
  $q->bindParam(':t', $t);
  $q->bindParam(':stamp', $now);
  $q->bindParam(':description', $description);
  $q->bindParam(':user', $user);
  $q->bindParam(':source', $target_file);

  
    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file) && $q->execute()) {
        ?>
  <div class="alert alert-success">
  <strong>Your post have been successfully uploaded.</strong><br>
  </div>
          <?php
      } else {
          echo "<div class='alert alert-danger'>Sorry, there was an error uploading your image post.</div>";
      }
  }
}elseif (isset($_POST['title2'])){
/*Video upload*/

  $title = secureTxt($_POST['title2']);
  $description = secureTxt($_POST['description']);
  $category = secureTxt($_POST['category']);
  $type = "video";

  $target_dir = "uploads/post/";
  $target_file = $target_dir . basename($_FILES["video"]["name"]);
  $uploadOk = 1;
  $videoFileType = pathinfo($target_file,PATHINFO_EXTENSION);

  //convert file type extension to lower case
  $videoFileType = strtolower($videoFileType);

  // Check if image file is a actual image or fake image
  if(isset($_POST["name"])) {
      $check = getimagesize($_FILES["video"]["tmp_name"]);
      if($check !== false) {
          
          $uploadOk = 1;
      } else {
          echo "<div class='alert alert-warning'>File is not a video.</div>";
          $uploadOk = 0;
      }
  }
  // Check file size
  if ($_FILES["video"]["size"] > 50000000) {
      echo "<div class='alert alert-warning'>Sorry, your video is too large.</div>";
      $uploadOk = 0;
  }
  // Allow certain file formats
  if($videoFileType != "mp4") {
      echo "<div class='alert alert-warning'>Sorry, only MP4 videos are allowed.</div>";
      $uploadOk = 0;
  }
  // Check if $uploadOk is set to 0 by an error
  if ($uploadOk == 0) {
      echo "<div class='alert alert-danger'>Sorry, your photo can not be uploaded.</div>";
  // if everything is ok, try to upload file
  } else {
 $q = $conn->prepare("INSERT INTO post (username, title, description, type, category, source, date, time, timestamp, views) VALUES (:user, :title, :description, :type, :category, :source, :d, :t, :stamp, '0')");

  $q->bindParam(':category', $category);
  $q->bindParam(':title', $title);
  $q->bindParam(':type', $type);
  $q->bindParam(':d', $d);
  $q->bindParam(':t', $t);
  $q->bindParam(':stamp', $now);
  $q->bindParam(':description', $description);
  $q->bindParam(':user', $user);
  $q->bindParam(':source', $target_file);

  
    if (move_uploaded_file($_FILES["video"]["tmp_name"], $target_file) && $q->execute()) {
        ?>
  <div class="alert alert-success">
  <strong>Your post was successfully uploaded.</strong><br>
  </div>
          <?php
      } else {
          echo "<div class='alert alert-danger'>Sorry, there was an error uploading your image post.</div>";
      }
  }
}

if (isset($_GET['type'])) {

  $type = $_GET['type'];
  require('pages/post/'.$type.'.php');
}
?>
                
              </div>

            </div>
            <!-- // END Panes -->

          </div>
          <!-- // END Tabbable Widget -->