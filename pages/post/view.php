 <div class="row grid js-masonry" data-masonry-options='{ "itemSelector": ".grid-item", "columWidth": 200 }' data-toggle="isotope">
 <div class="item col-xs-12 col-sm-6 col-lg-4 grid-item">
    <div class="panel panel-default paper-shadow" data-z="0.5">

      <div class="cover overlay cover-image-full hover">
        <span class="img icon-block height-150 bg-grey-200"></span>
        <a href="post?ref=add&type=image" class="view-post">
          <img src="img/logo.png" class="cover overlay cover-image-full hover" style="width: 100%; max-height: 400px">
        </a>

      </div>

      <div class="panel-body">
      <a href="post?ref=add&type=image" style="text-align: center;">
        <h4 class="text-headline margin-v-0-10">
          Add Post
        </h4>
      </a>
    </div>
    
    <hr class="margin-none" />
    <div class="panel-body">
        <div class="col-sm-12 col-lg-6">
          <a class="btn btn-default btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="post?ref=add&type=image" style="font-size: 11px;">
          <div class="icon-block img-circle bg-green-300" style="width:20px; height:20px; line-height: 5px">
            <i class="fa fa-image text-white" style="font-size: 11px;"></i>
          </div>
          Picture
        </a>
        </div>
        <div class="col-sm-12 col-lg-6">
          <a class="btn btn-default btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="post?ref=add&type=video" style="font-size: 11px;">
          <div class="icon-block img-circle bg-blue-300" style="width:20px; height:20px; line-height: 5px">
            <i class="fa fa-film text-white" style="font-size: 11px;"></i>
          </div>
          Video
        </a>
        </div>

    </div>

    </div>
  </div>
<?php

$q = $conn->prepare("SELECT * FROM post WHERE username = :user ORDER BY id DESC");
$q->bindParam(':user', $user);

if ($q->execute()) {
  $row = $q->fetch();

  if (empty($row['username'])) {
    ?>
<div class="alert alert-warning item col-xs-12 col-sm-12 col-lg-12 grid-item">
<strong>You have not created any post.</strong>
</div>
    <?php
  }else{

require('pages/post/user_post.php');

  }//checking the number of user post


}

?>
          </div>

</div>
          