<?php
//Update post
if(isset($_POST['update_post'])){
  $title = secureTxt($_POST['title']);
  $description = secureTxt($_POST['description']);
  $category = secureTxt($_POST['category']);
  $type = $_SESSION['type'];
  $postid = $_SESSION['postid'];
  $source = $_SESSION['source'];

  $target_dir = "uploads/post/";
  $type == 'image' ? $target_file = $target_dir . basename($_FILES["image"]["name"]) : $target_file = $target_dir . basename($_FILES["video"]["name"]);
  $uploadOk = 1;

  //Update image
  if($type == 'image'){
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

      if($_FILES["image"]["tmp_name"]){
        // Check if image file is an actual image or fake image
        if(isset($_POST["name"])) {
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                echo "<div class='alert alert-warning'>File is not an image.</div>";
                $uploadOk = 0;
            }
        }
         // Check file size
          if ($_FILES["image"]["size"] > 5000000) {
              echo "<div class='alert alert-warning'>Sorry, your photo is too large.</div>";
              $uploadOk = 0;
          }
          // Allow certain file formats
          if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
          && $imageFileType != "gif" ) {
              echo "<div class='alert alert-warning'>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</div>";
              $uploadOk = 0;
          }
          // Check if $uploadOk is set to 0 by an error
          if ($uploadOk == 0) {
              echo "<div class='alert alert-danger'>Sorry, your photo can not be uploaded.</div>";
          }else{
            // if everything is ok, try to upload file
            move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);

            //delete old image from folder
            @$oldimage = unlink($source);

            $q = $conn->prepare("UPDATE post SET username = :user, title = :title, description = :description, type = :type, category = :category, source = :source, date = :d, time = :t WHERE id = :postid");
            $q->bindParam(':source', $target_file);
          }
      }else{
        $q = $conn->prepare("UPDATE post SET username = :user, title = :title, description = :description, type = :type, category = :category, date = :d, time = :t WHERE id = :postid");
      }

      $q->bindParam(':postid', $postid);
      $q->bindParam(':category', $category);
      $q->bindParam(':title', $title);
      $q->bindParam(':type', $type);
      $q->bindParam(':d', $d);
      $q->bindParam(':t', $t);
      $q->bindParam(':description', $description);
      $q->bindParam(':user', $user);  
    if ($q->execute()) {
  ?>
      <div class="alert alert-success">
      <strong>Your post update was successful.</strong><br>
      </div>
    <?php
    } else {
        echo "<div class='alert alert-danger'>Sorry, there was an error updating your post.</div>";
    }
  }

  if($type == 'video'){
      if($_FILES["video"]["tmp_name"]){

        $videoFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        // Check if videeo file is an actual video or fake video
        if(isset($_POST["name"])) {
            $check = getimagesize($_FILES["video"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                echo "<div class='alert alert-warning'>File is not a video.</div>";
                $uploadOk = 0;
            }
        }
       // Check file size
        if ($_FILES["video"]["size"] > 50000000) {
            echo "<div class='alert alert-warning'>Sorry, your video is too large.</div>";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($videoFileType != "mp4") {
            echo "<div class='alert alert-warning'>Sorry, only mp4 files are allowed.</div>";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "<div class='alert alert-danger'>Sorry, your video can not be uploaded.</div>";
        }else{

          move_uploaded_file($_FILES["video"]["tmp_name"], $target_file);
          
          $q = $conn->prepare("UPDATE post SET username = :user, title = :title, description = :description, type = :type, category = :category, source = :source, date = :d, time = :t WHERE id = :postid");
          
          $q->bindParam(':source', $target_file);
        }
      }else{
        $q = $conn->prepare("UPDATE post SET username = :user, title = :title, description = :description, type = :type, category = :category, date = :d, time = :t WHERE id = :postid");
      }

      $q->bindParam(':postid', $postid);
      $q->bindParam(':category', $category);
      $q->bindParam(':title', $title);
      $q->bindParam(':type', $type);
      $q->bindParam(':d', $d);
      $q->bindParam(':t', $t);
      $q->bindParam(':description', $description);
      $q->bindParam(':user', $user);
      if ($uploadOk == 1) {
        $q->execute();
      ?>
        <div class="alert alert-success">
        <strong>Your post update was successful.</strong><br>
        </div>
      <?php
      } else {
        echo "<div class='alert alert-danger'>Sorry, there was an error updating your post.</div>";
      }
    }            
}//end of update post

//Delete Post
if(@$_GET['delkey']){

  $delpost = $_GET['delkey'];

  //delete from database
  $q1 = $conn->prepare("DELETE FROM post WHERE id = :delpost");
  $q1->bindParam(':delpost', $delpost);

  /*
    *delete image from folder
  */
  //retrive the source from database
  $q2 = $conn->prepare("SELECT source FROM post WHERE id = :delpost");
  $q2->bindParam(':delpost', $delpost);

  $q2->execute();

  $sourcename = $q2->fetch();

  @$sourceimage = unlink($sourcename['source']);

  if($q1->execute() && $sourceimage){
      echo "<div class='alert alert-success'>Success, post was deleted successfully.</div>";
  }
  else{
     echo "<div class='alert alert-danger'>Error! Post delete failed.</div>";
  }
}//end of delete post


$q = $conn->prepare("SELECT * FROM post WHERE username = :user ORDER BY id DESC");
$q->bindParam(':user', $_SESSION['logged_user']);

$q->execute();

?>

<!--===========-->
<?php
while ($row = $q->fetch()) {
  ?>
<div class="item col-xs-12 col-sm-6 col-lg-4  grid-item">
  <div class="panel panel-default paper-shadow" data-z="0.5">

    <?php if($row['type'] == 'image'){ ?>
    
            <div class="embed-responsive embed-responsive-16by9">
              <img class="embed-responsive-item" src="<?php echo $row['source']; ?>">
            </div>
    <?php }else{ ?>
      <div class="embed-responsive embed-responsive-16by9">
        <video>
          <source src="<?php echo $row['source']; ?>" type="video/mp4"></source>
          Your browser does not support the video tag.
        </video>
      </div>
    <?php } ?>

    <div class="panel-body">
      <h4 class="text-headline margin-v-0-10" style="font-size: small;font-weight: 700;text-align: center;">
        <a href="explore?post=<?php echo $row['id']; ?>"><?php echo $row['title']; ?></a>
      </h4>

    </div>
    <hr class="margin-none" />
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12 col-lg-5">
          <!-- Display Edit modal -->
          <button class="btn btn-default btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated data-toggle="modal" data-target="#edit_modal" onclick="editpost(<?php echo $row['id']; ?>);"><i class="fa fa-fw fa-pencil"></i> Edit post</button>
        </div>
        <div class="col-sm-12 col-lg-5">
          <!-- Display delete modal -->
          <button class="btn btn-danger" data-toggle="modal" data-target="#delete_modal" onclick="delkey(<?php echo $row['id']; ?>,'<?php echo $row['title']; ?>');"><i class="fa fa-fw fa-trash"></i> Delete post</button>
        </div>
      </div>
    </div>

  </div>
</div>
            
  <?php
}//end of while statement....
?>
</div>

<div style="clear: both"></div>