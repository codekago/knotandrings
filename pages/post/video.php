<h3>Video post</h3>
<form action="<?php echo htmlspecialchars('post?ref=add&type=video'); ?>" method="post" enctype="multipart/form-data"><br>
                <div class="form-group">
                    <label>Upload Video</label>
                      <div class="media v-middle">
                        <div class="media-left" id="load_video">
                        </div>
                        <div class="media-body">
                        <input type="file" required id="video" name="video" class="btn btn-white btn-sm paper-shadow relative"/>
                        </div>
                      </div>
                  </div>
                  <div class="form-group">
                    <label>Post Category</label>
                    <select class="form-control" name="category">
                      <option value="White_wedding">White wedding</option>
                      <option value="Traditional_marriage">Traditional marriage</option>
                      <option value="Accessories_designers">Accessories designers</option>
                      <option value="Master_of_ceremonies">Master of ceremonies (mc)</option>
                      <option value="Photographers">Photographers</option>
                      <option value="Decorators">Decorators</option>
                      <option value="Disc_jockey">Disc jockey (dj)</option>
                      <option value="Cake_and_desert">Cake and desert</option>
                      <option value="Catering_and_drinks">Catering and drinks</option>
                      <option value="Bridal_couture">Bridal couture</option>
                      <option value="souveniers">Souveniers</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Post Title</label>
                    <input type="text" name="title2" id="title" placeholder="Title of post" class="form-control" value="<?php
if (isset($_POST['title2']) && $uploadOk == 0) {
  echo $_POST['title2'];
}
                    ?>" />
                  </div>
                  <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" id="description" cols="30" rows="5" class="form-control" placeholder="Description of post"><?php
if (isset($_POST['title2']) && $uploadOk == 0) {
  echo $_POST['description'];
}
                    ?></textarea>
                  </div>
                <div class="text-right">
                  <input type="submit" class="btn btn-primary" />
                </div>
                </form>