<?php
	include('../include/connect.php');
	include('../include/connect2.php');

	$search = $_REQUEST['search']."%";

	$q = $conn->prepare("SELECT * FROM post WHERE title LIKE :search");
	$q->bindParam(':search', $search);

	$q->execute();

	function limit_words($string, $word_limit){
	    $words = explode(" ",$string);
	    return implode(" ",array_splice($words,0,$word_limit));
	}
	echo "<div class='pull-left col-sm-12 col-xs-12' style='padding: 5px;'>Search topics found <span class='badge pull-right'>".$q->rowCount()."</span></div>";

	while($row = $q->fetch()){
		$title = $row['title'];
		$category = $row['category'];
		$username = $row['username'];		
		$post_id = $row['id'];
?>
		<li>
			<a href='?<?php echo "post=".$post_id; ?>' style="padding: 3px 3px;"><?php echo $title; ?></a>
				<blockquote class="blockquote" style="border-left: 2px solid #eeeeee; font-size: 10.2px; padding: 0px 5px;">
					<?php echo "Category: ".$category; ?>
				</blockquote>
		</li>
		<li role="presentation" class="divider"></li>
<?php 
	}
?>