<div class="container" style="margin-top: 40px;">

    <div class="category-section">
      <div class="row">
        <div class="col-md-3">

<?php require('pages/explore_sidemenu.php'); ?>

        </div>

        <div class="col-md-9">
<?php

if (isset($_GET['category'])) {
	
		if($_GET['category'] != ''){
		    $category = array("all", "white_wedding", "traditional_marriage", "accessories_designers", "master_of_ceremonies", "photographers", "decorators", "disc_jockey", "cake_and_desert", "catering_and_drinks", "bridal_couture", "souveniers");
		    if(in_array($_GET['category'], $category)){
		        $category = $_GET['category'];
		        
		    }else{
		  $category = "404";
		}
		}else{
		    $category = "all";
		}

		require('pages/category.php');

}elseif(isset($_GET['post'])) {
require('pages/view.php');
}else{
	$category = "all";
	require('pages/image_category.php');
}



?>
        </div>

      </div>
    </div>

  </div>