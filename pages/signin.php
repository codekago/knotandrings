<div id="body" style="height: 100%;">

<div class="container">
        <header>
            <h1 class="logo"><a href="signup"><img src="img/logo.png" height="70px" /></a></h1>
            <div class="nav" style="float: right;">
                <p class="ask">New to Knot&Rings?</p>
                <a href="signup" class="btn btn-default signup" style="padding: 15px 15px;">Sign Up</a>
            </div>
        </header>

        <section class="">
            <h2>LETS RATE YOU</h2>
            <p class="home_text_body">Knot and Rings is the world's first online wedding rating magazine, focused on showcasing iconic wedding moments.</p>
            <center>
                <!-- <li class="social_item"><a href="#" class="fb homefb"><span class="socicon socicon-facebook"></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sign in with Facebook</a></li> -->

                <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
</fb:login-button>
<p id="status"></p>
            </center>
            <?php
            if (isset($_POST['user'])) {
               $user = secureTxt($_POST['user']);
               $pwd = securePwd($_POST['pwd']);

               $q = $conn->prepare("SELECT * FROM account WHERE username = :user");
               $q->bindParam(':user', $user);

               $q->execute();

               $row = $q->fetch();

               if ($row['username'] == $user) {
                
                //checking to make sure useraccount have been verified
                if ($row['status'] == 0) {
                  ?>
<div class="alert alert-danger">
<strong>Your account is not yet verified.</strong><br>
A verification link was sent to your inbox.
</div>
<?php
                }else{
$_SESSION['logged_user'] = $row['username'];
                $log = $conn->prepare("INSERT INTO logs (username, log_date, log_time) VALUES (:user, :log_date, :log_time)");
                $log->bindParam(':user', $user);
                $log->bindParam(':log_date', $d);
                $log->bindParam(':log_time', $t);
                $log->execute();
                
                    ?>
<div class="alert alert-success">
<strong>Welcome <?php echo $user; ?>!</strong><br>
Redirecting you to your dashboard.
                   </div>
                   <?php
                   sleep(2);
                   header("location: explore");
                }//login the user
                
         
               }else{
?>
<div class="alert alert-danger">
<strong>Incorrect login details.</strong>
</div>
<?php
               }//login verification check....



            }else{
                echo '<p class="small">or via email</p>';
            }//end of if statement
            ?>

            <form role="form" action="signin" method="post">
              <div class="form-group" style="margin-bottom: 0px;">
                <input type="text" required class="form-control" autofocus name="user" placeholder="Your Username">
                <input type="password" required class="form-control" name="pwd" placeholder="Your Password">
              </div>
            <a href="password_reset" class="forgot" style="margin-bottom: 5px;">Forgot username or password?</a>  
              <button type="submit" class="btn btn-warning">Sign in</button>
            </form>

        </section>

        
        <ul class="bottom_links">
            <li><a href="#">Terms of Use</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">About Us</a></li>
        </ul>

    </div>

    </div>