<?php
if (isset($_GET['post'])) {
  $id = secureTxt($_GET['post']);
  $us = secureTxt($_SESSION['logged_user']);

  $q51 = $conn->prepare("SELECT * FROM post_views WHERE post_id = :id AND username = :user");
  $q51->bindParam(':id', $id);
  $q51->bindParam(':user', $us);
  $q51->execute();

  $we = $conn->prepare("SELECT * FROM post WHERE id = :id");
  $we->bindParam(':id', $id);
    $we->execute();
$row5 = $we->fetch();
      $post_views = $row5['views'];

  if ($q51->rowCount() != 0) {

$views = $post_views;


  }else{
    $insert = $conn->prepare("INSERT INTO post_views (post_id, username) VALUES (:id, :user)");
      $insert->bindParam(':id', $id);
      $insert->bindParam(':user', $us);

      $view = $post_views + 1;
      
      $views_update = $conn->prepare("UPDATE post SET views = :views WHERE id = :id");
      $views_update->bindParam(':id', $id);
      $views_update->bindParam(':views', $view);

      if ($insert->execute() && $views_update->execute()) {
        $we = $conn->prepare("SELECT * FROM post WHERE id = :id");
    $we->bindParam(':id', $id);
    $we->execute();
$row5 = $we->fetch();
      $post_views = $row5['views'];
      $views = $post_views;

      }else{
        $views = 1;
      }

  }

  

 

  $q = $conn->prepare("SELECT * FROM post WHERE id = :id");
  $q->bindParam(':id', $id);
  $q->execute();

  $row = $q->fetch();

  $title = $row['title'];
  $desc = $row['description'];
  $user = $row['username'];
  $cat = $row['category'];
  $type = $row['type'];
  $img = $row['source'];
  $status = $row['status'];

  ?>

<!-- report modal -->
<div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">

      <form role="form" id="reportForm">
    <div class="modal-content">
    <div class="modal-header" style="background: #F68634; padding-top: 5px; padding-bottom: 5px;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4>Help Us Understand What's Happening</h4>
        </div>
<div class="modal-body" id="reportAlert">
        <div class="row">
  <div class="col-md-4">
<img src="<?php echo $img; ?>" style="opacity: 0.7;" height="100%" width="100%" class="img-thumbnail" alt="post image" /><br>
By <a href="user_view?username=<?php echo $row['username']; ?>" target="_blank"><?php echo $user; ?></a>
  </div>
  <div class="col-md-8">
<b><h4 style="margin-top: 0px;">Why don't you want to see this post?</h4></b>
<input type="hidden" value="<?php echo $id; ?>" id="post_id" />
<label>
    <input type="radio" name="optionsRadios" class="reason" value="It's annoying or not interesting">
    It's annoying or not interesting
  </label><br>
  <label>
  <input type="radio" name="optionsRadios" class="reason" value="I'm in this post and I don't like it">
    I'm in this post and I don't like it
  </label><br>
  <label>
  <input type="radio" name="optionsRadios" class="reason" value="I think it shouldn't be on Knot and rings">
    I think it shouldn't be on Knot and rings
  </label><br>
  <label>
  <input type="radio" name="optionsRadios" class="reason" value="It's a spam">
    It's a spam
  </label>

  </div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" id="continueBtn" disabled class="btn btn-warning">Continue</button>
      </div>
    </div>
      </form>
  </div>
</div>
<script src="js/jquery.js"></script>
<script>
$('document').ready(function() {
$('.reason').click(function() {
  var reason = $(this).val();
  localStorage.setItem('reason', reason);
  
  $('#continueBtn').removeAttr('disabled');
});

$('#continueBtn').click(function() {
  var reason = localStorage.getItem('reason');
  var id = $('#post_id').val();

  $('body').oLoader({
            wholeWindow: true, //makes the loader fit the window size
            lockOverflow: true, //disable scrollbar on body

            backgroundColor: '#000',
            fadeInTime: 1000,
            fadeLevel: 0.4,
            image: 'img/loader.gif',
            //hideAfter: 1500
        });

  $(this).css('display', 'none');

$("#reportAlert").load('include/report.php', {'report': reason, 'id': id});
});

});
</script>

          <div class="width-300-md width-100pc-xs paragraph-inline" style="float: none; width: 100%;">
<div id="alert"></div>

              <div class="embed-responsive embed-responsive-16by9">
      <?php if($row['type'] == 'image'){ ?>
      <img class="embed-responsive-item" src="<?php echo $img; ?>">
      <?php }else{ ?>
       <i class="fa fa-play fa-3x video_button" style="visibility: hidden;" id='<?php echo "btn_".$row['id']; ?>' onmouseenter="show_btn(<?php echo $row['id']; ?>);" onmouseout="hide_btn(<?php echo $row['id']; ?>);" onclick="video_play(<?php echo $row['id']; ?>);"></i>
       <video id="<?php echo "vid_".$row['id']; ?>" class="embed-responsive-item" onmouseenter="show_btn(<?php echo $row['id']; ?>);" onmouseout="hide_btn(<?php echo $row['id']; ?>);"> 
          
          <source src="<?php echo $row['source']?>" type="video/mp4"></source>

          Your browser does not support the video tag.
        </video>
      <?php } ?>
    </div>
    
          </div>
          <a style="display: none;" class="btn btn-info" href="javascript:;" onclick="skip_ad(<?php echo $row['id']; ?>);" id="skip_<?php echo $row['id']; ?>">Skip ad</a><br><br>
          <h4 class="text-headline margin-v-0-10"><?php echo $row['title']; ?></h4>
          <p><?php echo $desc; ?></p>
          <br/>
          <p class="margin-none">

            <span class="label bg-gray-dark"><?php
                      echo timeAgo($row['timestamp']);
 ?></span>
 <span class="label bg-gray-dark"><?php

 if ($views == 0 || $views == 1) {
   echo number_format($views)." view";
 }else{
echo number_format($views)." views";
 }
 ?></span>
 
            <span class="pull-right">
                <a class="btn btn-white paper-shadow relative" title="report post" data-z="1" href="#reportModal" data-toggle="modal"><i class="glyphicon glyphicon-eye-close"></i> report</a>
              </span>
              <div style="clear: both"></div>
          </p>

        <div class="page-section">
          <div class="row">

              <!-- <div class="pull-right">
                <a class="btn btn-white btn-circle paper-shadow relative" data-z="1" href="#"><i class="glyphicon glyphicon-thumbs-down"></i></a>
              </div> -->

              <h2 class="text-headline margin-none">Reviews</h2>
              <p class="text-subhead text-light">What people say about this post</p>
              <div class="slick-basic slick-slider" data-items="1" data-items-lg="1" data-items-md="1" data-items-sm="1" data-items-xs="1">
              <?php
                $q = $conn->prepare("SELECT * FROM rating WHERE post_id = :id ORDER BY id DESC");
  $q->bindParam(':id', $id);

  $q->execute();

  $count = $q->rowCount();
  if ($count > 0) {
    while ($row = $q->fetch()) {
    ?>
<div class="item">
                  <div class="testimonial">
                    <div class="panel panel-default">
                      <div class="panel-body">
                      <b><?php echo $row['title']; ?></b><br>
                        <p><?php echo $row['review']; ?></p>
                      </div>
                    </div>
                    <div class="media v-middle">
                      <div class="media-left">
                      <?php

$q1 = $conn->prepare("SELECT * FROM profile WHERE username = :user");
$q1->bindParam(':user', $row['username']);
$q1->execute();

while ($row2 = $q1->fetch()) {
  ?>
<img src="<?php echo $row2['image']; ?>" alt="User image" class="img-circle width-40" />
  <?php
}
                      ?>
                        
                      </div>
                      <div class="media-body">
                        <p class="text-subhead margin-v-5-0">
                          <strong><a href="user_view?username=<?php echo $row['username']; ?>"><?php echo $row['username']; ?><span class="text-muted"></span></a></strong>
                        </p>
                        <p class="small">
                          <?php
if ($row['rate'] == 1) {
  ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($row['rate'] == 2) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($row['rate'] == 3) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($row['rate'] == 4) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($row['rate'] == 5) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
  <?php
}
                          ?>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
    <?php
  }
  }else{
    ?>
<center>
<div class="alert alert-warning">
<strong>This post have not been rated</strong><br>
Be the first to rate it.
<p><a class="btn btn-success btn-lg paper-shadow relative" data-z="1" data-hover-z="2" data-animated  href="#reviewModal" data-toggle="modal">Rate post</a></p>
</div>
</center>
    <?php
  }
                ?>
              </div>
            
          </div>

        </div>
  <?php
}else{
  
}

