<?php
$user = secureTxt($_GET['username']);

	$q = $conn->prepare("SELECT * FROM post WHERE username = :user ORDER BY id DESC");
	$q->bindParam(':user', $user);

?>
<div class="row grid js-masonry"
  data-masonry-options='{ "itemSelector": ".grid-item", "columWidth": 200 }' data-toggle="isotope">
  <?php

$q->execute();

function limit_words($string, $word_limit)
{
    $words = explode(" ",$string);
    return implode(" ",array_splice($words,0,$word_limit));
}

while ($row = $q->fetch()) {
  ?>
<div class="item col-xs-12 col-sm-6 col-lg-6 grid-item">
              <div class="panel panel-default paper-shadow" data-z="0.5">

                <a href="<?php if(isset($view_type) && $view_type == 'video'){ echo 'video'; }else{ echo 'explore'; }?>?post=<?php echo $row['id']; ?>" id="<?php echo $row['id']; ?>" class="view-post">
    <?php if($row['type'] == 'image'){ ?>
    <img src="<?php echo $row['source']; ?>" class="cover overlay cover-image-full hover" style="width: 100%; max-height: 400px">
    <?php }else{ ?>
    <div class="embed-responsive embed-responsive-16by9">
      <i class="fa fa-play fa-3x video_button" style="visibility: hidden;" id='<?php echo "btn_".$row['id']; ?>' onmouseenter="show_btn(<?php echo $row['id']; ?>);" onmouseout="hide_btn(<?php echo $row['id']; ?>);"></i>
      <video class="embed-responsive-item" onmouseenter="show_btn(<?php echo $row['id']; ?>);" onmouseout="hide_btn(<?php echo $row['id']; ?>);">
        <source src="<?php echo $row['source']?>" type="video/mp4"></source>
        Your browser does not support the video tag.
      </video>
    </div>
    <?php } ?>
    </a>

                <div class="panel-body" style="padding-top: 0px;">
                  <h4 class="text-headline margin-v-0-10"><a href="explore?ref=post&id=<?php echo $row['id']; ?>" id="<?php echo $row['id']; ?>" class="view-post"><?php echo $row['title']; ?></a></h4>

                  <p class="small margin-none">
                  <?php
                  $id = $row['id'];
//echo $id;
  $q3 = $conn->prepare("SELECT rate FROM rating WHERE post_id = :id");
  $q3->bindParam(':id', $id);

  $q3->execute();
  $count = $q3->rowCount();


  if ($count > 0) {
    
      /*$array[] = $row3['rate'];
      $r2 = array_sum($array);
      echo $r2;*/

      $sum = 0;
      $ids_array = array();
      while ($row3 = $q3->fetch()) {
        
   $sum+= $row3['rate'];

      }

      
$rate = round($sum/$count);
$rate2 = round($sum/$count, 1);

?>
<b>Reviews: <?php echo $count; ?>  &nbsp;
Rate: <?php echo $rate2; ?>
</b>
<?php

if ($rate == 1) {
  ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rate == 2) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rate == 3) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rate == 4) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}elseif ($rate == 5) {
    ?>
<span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star text-yellow-800" style="color: #F58634;"></span>
  <?php
}else{
  ?>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
}
    
  }else{
    ?>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;" ></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800" style="color: #F58634;"></span>
  <?php
  }

  

?>
                  </p>

                </div>
                <hr class="margin-none" />
                <div class="panel-body">
                  <p>
                  <?php 
                  $content =  $row['description'];

                  if (str_word_count($content) > 35) {
                    echo limit_words($content,35)."...";
                  }else{
                    echo $content;
                  }
                  
                  ?>
                  </p>

                  <div class="media v-middle">
                    <div class="media-left">
                      <img src="<?php
                      $q1 = $conn->prepare("SELECT * FROM profile WHERE username = :user");
$q1->bindParam(':user', $row['username']);
$q1->execute();

while ($row2 = $q1->fetch()) {
  echo $row2['image'];
}

                      ?>" alt="profile image" class="img-circle width-40" />
                    </div>
                    <div class="media-body">
                      <h4 style="margin-bottom: 0px;"><a href="user_view?username=<?php echo $row['username']; ?>"><?php echo $row['username']; ?></a>
                        <br/>
                      </h4>
                      <b>Date:</b> <?php echo $row['date']; ?> <b>Time:</b> <?php echo $row['time']; ?>
                    </div>
                  </div>

                </div>

              </div>
            </div>
  <?php
}
  ?>
            
            
          </div>

<!--Message Modal-->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content alert-info" style="background-color: #d9edf7 !important; border-color: #bce8f1 !important;">
      <form role="form" id="msgForm">
<div class="modal-header" style="border-bottom: none; padding-bottom: 5px;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Message to <span id="msgUser"></span></h4>
        <p id="msgAlert"></p>
      </div>
      <div class="modal-body" style="padding-top: 0px; padding-bottom: 0px;">
        <div class="form-group">
<textarea id="message" required placeholder="Write your message" row="3" class="form-control"></textarea>
        </div>
      </div>
      <div class="modal-footer" style="border-top: none; padding-top: 5px;">
        <button type="submit" class="btn btn-info">Submit</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>