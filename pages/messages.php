<div class="container">

    <div class="page-section">
      <div class="row">

      <div class="col-md-3">
<?php require('pages/sidemenu.php'); ?>
</div>

        <div class="col-md-9">

          <div class="media messages-container media-clearfix-xs-min media-grid">
            <div class="media-left">
              <div class="messages-list">
                <div class="panel panel-default paper-shadow" data-z="0.5" data-scrollable-h>
                <center>
<img src="img/loader.gif" style="height: 25px; margin: 10px 0px;" id="conversationLoader" />
                </center>
                  <ul class="list-group" id="conversation_load" style="max-height: 400px;  overflow: auto;"></ul>
                </div>
              </div>
            </div>
            <div class="media-body">

              <form id="sendMessage">
<div class="form-group">
                <div class="input-group">
                  <!-- /btn-group -->
                  <input type="text" id="userMessage" autofocus required class="form-control share-text" placeholder="Write message..." />
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary">
                      <i class="fa fa-envelope"></i> Send
                    </button>
                  </div>
                </div>
                <!-- /input-group -->
              </div>
              </form>
              <span id="messageAlert"></span>

<center>
<img src="img/loader.gif" style="height: 25px; margin-bottom: 10px;" id="messagesLoader" />
</center>
<span id="messageLoad"></span>
              
            </div>
          </div>

          <br/>
          <br/>

        </div>

      </div>
    </div>

  </div>