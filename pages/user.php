<?php

if (!isset($_SESSION['logged_user'])) {
  header('location: signin');
}

if($_GET['page'] != ''){
    $pages = array("user_view", "profile", "gallery", "messages", "explore", "logout", "video", "post", "terms", "policy");
    if(in_array($_GET['page'], $pages)){
        $page = $_GET['page'];
        
    }else{
  $page="explore";
}
}else{
    $page="explore";
}

?>

<!-- Fixed navbar -->
  <div class="navbar navbar-default navbar-fixed-top navbar-size-large navbar-size-xlarge paper-shadow" data-z="0" data-animated role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="navbar-brand navbar-brand-logo">
          <a href="explore">
            <img src="img/logo.png" height="50px" />
          </a>
        </div>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="main-nav">
        <ul class="nav navbar-nav navbar-nav-margin-left">
        <li <?php
if ($page == 'explore') {
  echo 'class="active"';
}
        ?>><a href="explore">Explore</a></li>
        <li <?php
if ($page == 'gallery') {
  echo 'class="active"';
}
        ?>><a href="gallery">Gallery</a></li>
        <li <?php
if ($page == 'video') {
  echo 'class="active"';
}
        ?>><a href="video">Video</a></li>
        <li <?php
if ($page == 'profile' || $page == 'post' || $page == 'messages') {
  echo 'class="active"';
}
        ?>><a href="profile">My Account</a></li>
        </ul>
        <div class="navbar-right">
          <ul class="nav navbar-nav navbar-nav-bordered navbar-nav-margin-right">
            <!-- user -->
            <li class="dropdown user active">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img <?php
if ($row['image'] == '') {
  echo 'src="images/user.png"';
}else{
  echo 'src="'.$row['image'].'"';
}
                ?> alt="" class="img-circle" style="height: 30px;" /> <?php echo $_SESSION['logged_user']; ?><span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li <?php
if ($page == 'post') {
  echo 'class="active"';
}
        ?>><a href="post"><i class="fa fa-bullhorn"></i> Post</a></li>
                <li <?php
if ($page == 'profile') {
  echo 'class="active"';
}
        ?>><a href="profile"><i class="fa fa-user"></i> Profile</a></li>
                <li <?php
if ($page == 'messages') {
  echo 'class="active"';
}
        ?>><a href="messages"><i class="fa fa-envelope"></i> Messages</a></li>
                <li><a href="logout"><i class="fa fa-sign-out"></i> Logout</a></li>
              </ul>
            </li>
            <!-- // END user -->
          </ul>
          <a href="logout" class="navbar-btn btn btn-primary">Logout</a>
        </div>
      </div>
      <!-- /.navbar-collapse -->

    </div>
  </div>
<?php
$img = $row['image'];
?>
  <div class="parallax overflow-hidden bg-blue-400 page-section third">
    <div class="container parallax-layer" data-opacity="true">
      <div class="media v-middle">
        <div class="media-left text-center">
          <a href="#">
            <img <?php
if ($row['image'] == '') {
  echo 'src="images/user.png"';
}else{
  echo 'src="'.$row['image'].'"';
}
                ?> alt="<?php echo $user; ?>" class="img-circle width-80" style="height: 80px;" />
          </a>
        </div>
        <div class="media-body">
          <?php 
if ($name == '') {
  ?>
  <h1 class="text-white text-display-1 margin-v-0">
  <?php echo $user; ?>
  </h1>
          <p class="text-subhead">You don't have a profile with us. <a class="link-white text-underline" href="profile?ref=edit">Create profile</a></p>
        </div>
  <?php
}else{
?>
<h1 class="text-white text-display-1 margin-v-0">
  <?php echo $name; ?>
  </h1>
          <p class="text-subhead"><a class="link-white text-underline" href="profile?ref=view">Welcome <?php echo $user; ?>!</a></p>
        </div>
  <?php
}
           ?>
      </div>
    </div>
  </div>
  

<?php require('pages/'.$page.'.php'); ?>
<!--Edit Modal-->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content" style="background-color: #FAFCFC;">
      <div class="modal-body">
        <h3>Edit post</h3>
        <form action="?" method="post" enctype="multipart/form-data"><br>
        <!--Load content-->
        <div id="edit_post" style="clear:both;"><img src="img/loader.gif" style="margin: 40px auto 40px 45%"></div>
        <button type="submit" style="width: 48%;" class="btn btn-info" name="update_post">Save</button>
        <button type="button" class="btn btn-primary"  style="width: 48%; float: right;" data-dismiss="modal">Cancel</button>
        </form>
      </div>
    </div>
    </div>
  </div>
</div>
<!--End of edit post modal-->

<!--Delete modal-->
<div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" style="background-color: #FFB2B2;">
      <div class="modal-body" style="color:#E71C1C;">
        Do you want to delete the post <strong id="post-title"></strong>
      </div>

        <a style="width: 48%;" class="btn btn-danger btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated id="delkey" onclick="delpost();">Delete</a>
        <button type="button" class="btn btn-primary"  style="width: 48%; float: right;" data-dismiss="modal">Cancel</button>
    </div>
  </div>
</div>
<!--End of delete post modal-->

<script type="text/javascript">
  function delkey(e,f){
    $('#delkey').attr('ref',e);
    $('#post-title').text(f);
  }

  function delpost(){
    var del = $('#delkey').attr('ref');
    window.location = "post?delkey="+del;
  }

  function editpost(e){
   $("#edit_post").load("pages/edit_post.php",{'postid': e});
  }
</script>
    <!-- Footer -->
  <footer class="footer">
    <strong>KnotsandRings</strong> &copy; Copyright 2015
  </footer>
  <!-- // Footer -->
  <div id="top" title="Top"><i class="fa fa-arrow-up"></i></div>
  <style>
#top {
  position: fixed;
  right: 20px;
  bottom: 20px;
  width: 24px;
  height: 24px;
  background-size: 100%;
  z-index: 9999999999;
  opacity: 0.2;
}
  </style>
<!-- Modal -->
<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
        <form role="form" id="rate-form">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: none;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <center>
        <?php
$user = $_SESSION['logged_user'];

   $q4 = $conn->prepare("SELECT * FROM profile WHERE username = :user");
   $q4->bindParam(':user', $user);
   $q4->execute();

   $row4 = $q4->fetch();
   $imgg = $row4['image'];
        ?>
<img class="img-rounded" src="<?php echo $imgg; ?>" style="height: 50px; width: 50px;" /> <h4 class="modal-title" id="myModalLabel">Review by <?php echo $_SESSION['logged_user']; ?></h4>

<p class="small margin-none">
            <span class="fa fa-fw fa-star-o text-yellow-800 1" style="color: #F58634; cursor: pointer;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800 2"style="color: #F58634; cursor: pointer;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800 3"style="color: #F58634; cursor: pointer;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800 4"style="color: #F58634; cursor: pointer;"></span>
            <span class="fa fa-fw fa-star-o text-yellow-800 5"style="color: #F58634; cursor: pointer;"></span>
</p>
<p id="rate-status" style="color: #F58634; margin-bottom: 0px;"></p>

        </center>
        
      </div>
      <div class="modal-body">
        <center>
        <p id="rate-alert"></p>
        </center>
          <div class="form-group">
<label>Title</label>
<input type="hidden" id="rate-user" value="<?php echo $_SESSION['logged_user']; ?>" />
<input type="hidden" id="rate-id" value="<?php echo $id; ?>" />
<input type="text" required id="rate-title" class="form-control" />
          </div>
          <div class="form-group">
<label>Description</label>
<textarea id="rate-description" required class="form-control" row="3"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

        </form>
  </div>
</div>

