<?php

include('include/connect.php');
include('include/connect2.php');

if (isset($_SESSION['logged_user'])) {
   $page = "user";

   $user = $_SESSION['logged_user'];

   $q = $conn->prepare("SELECT * FROM profile WHERE username = :user");
   $q->bindParam(':user', $user);
   $q->execute();

   $row = $q->fetch();


   $email = $row['email'];
   $name = $row['name'];
   $gender = $row['gender'];
   $web = $row['website'];
   $dob = $row['dob'];
   $img = $row['image'];
   $phone = $row['phone'];

}else{

if($_GET['page'] != ''){
    $pages = array("about", "explore", "contact", "signin", "signup", "terms", "policy");
    if(in_array($_GET['page'], $pages)){
        $page = $_GET['page'];
        
    }else{
  $page="signup";
}
}else{
    $page="signup";
}

}//end of login check
?>
<!DOCTYPE html>
<html class="full" lang="en">
<!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->

<head>

    
<?php
if (isset($_SESSION['logged_user'])) {
   
if (isset($_GET['post'])) {

  $idee = $_GET['post'];

  $qee = $conn->prepare("SELECT * FROM post WHERE id = :id");
  $qee->bindParam(':id', $idee);
  $qee->execute();

  $rowee = $qee->fetch();

  $titleee = $rowee['title'];
  $descee = $rowee['description'];
  $typepost = $rowee['type'];
  $imgpost = $rowee['source'];

  function limit_words($string, $word_limit)
{
    $words = explode(" ",$string);
    return implode(" ",array_splice($words,0,$word_limit));
}

  ?>
<title><?php echo $titleee; ?></title>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="WEDDING, MAGAZINE, UYO, AKWA IBOM STATE, TRADITIONAL MARRIAGE, BACHELOR, BRIDAL SHOWER, SOUVENIERS, WHITE WEDDING, ACCESSORIES DESIGNERS, MASTER OF CEREMONIES, MC, PHOTOGRAPHERS, DECORATORS, DISC JOCKEY, CAKE, CAKE AND DESERT, CATERING, DRINKS, CATERING AND DRINKS, BRIDAL COUTURE, BRIDE, GROOM, WIFE, HUSBAND">
    <meta name="author" content="knotandrings.com">
    <meta property="og:title" content="<?php echo $titleee; ?>" />
<meta property="og:url" content="http://knotandrings.com" />
<meta property="og:image" content="http://knotandrings.com/<?php echo $imgpost; ?>" />
<meta property="og:description" content="<?php 
 if (str_word_count($descee) > 20) {
                    echo limit_words($descee,20).'...';
                  }else{
                      echo $descee;
                  }
     ?>" />
<meta property="og:site_name" content="knotandrings.com" />
  <?php

}

   }else{
    ?>
<title>Knot and Rings</title>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Knot and Rings is the world's first online wedding rating magazine, focused on showcasing iconic wedding moments and rewarding couples and different wedding vendors with gifts who make the monthly top 5 listing. ">
    <meta name="keywords" content="WEDDING, MAGAZINE, UYO, AKWA IBOM STATE, TRADITIONAL MARRIAGE, BACHELOR, BRIDAL SHOWER, SOUVENIERS, WHITE WEDDING, ACCESSORIES DESIGNERS, MASTER OF CEREMONIES, MC, PHOTOGRAPHERS, DECORATORS, DISC JOCKEY, CAKE, CAKE AND DESERT, CATERING, DRINKS, CATERING AND DRINKS, BRIDAL COUTURE, BRIDE, GROOM, WIFE, HUSBAND">
    <meta name="author" content="knotandrings.com">
    <?php
   }
?>
    

    <style type="text/css">
      .video_button{
        z-index: 1;
        position: absolute;
        margin: 25% 45%;
        color: rgb(48, 44, 44);
        border-radius: 80px;
        border: 3px solid rgb(48, 44, 44);
        padding: 10px;
        box-shadow: 0px 0px 4px;
      }
    </style>
        <?php
if (isset($_SESSION['logged_user'])) {
   ?>
<link href="user/css/vendor/all.css" rel="stylesheet">
<link href="user/css/app/app.css" rel="stylesheet">
<link href="libraries/dropzone/basic.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/lightbox.css">
   <?php
}else{
?>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <linK href="libraries/owl-carousel/owl.carousel.css" rel="stylesheet" /> <!-- Core Owl Carousel CSS File  * v1.3.3 -->
    <linK href="libraries/owl-carousel/owl.theme.css" rel="stylesheet" /> <!-- Core Owl Carousel CSS Theme  File  * v1.3.3 -->
    <link href="libraries/flexslider/flexslider.css" rel="stylesheet" /> <!-- flexslider -->
    <link href="libraries/fonts/font-awesome.min.css" rel="stylesheet" />
    <link href="libraries/animate/animate.min.css" rel="stylesheet" />
    <link href="css/components.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/media.css" rel="stylesheet" />

    <link rel="stylesheet" href="css/socicons.css">
    <link href="css/full.css" rel="stylesheet">
    <link rel="stylesheet" href="js/vegas/vegas.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php
}

?>
    
 <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "2b40a953-6a38-4a50-8db4-5120bd40788a", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</head>

<body     <?php
if (isset($_SESSION['logged_user'])) {
   ?>

   <?php
}else{
?>
data-offset="200" data-spy="scroll" data-target=".primary-navigation"
<?php
}
?>
>

<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '1017989174892828',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
      console.log('Successful login for: ' + response.public_profile);
      console.log('Successful login for: ' + response.email);
      console.log('Successful login for: ' + response.image);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.email + '!';
    });
  }
</script>

<?php require('pages/'.$page.'.php'); ?>


    <?php
if (isset($_SESSION['logged_user'])) {
   ?>
   <script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "html",
      skins: {
        "default": {
          "primary-color": "#42a5f5"
        }
      }
    };
  </script>
  <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/lightbox.min.js"></script>
  <script src="user/js/vendor/all.js"></script>
<script src="user/js/app/app.js"></script>
<script src="libraries/dropzone/dropzone.min.js"></script>
<script src="js/jquery.oLoader.js"></script>
<script src="js/user.js"></script>
<script src="js/jquery.masonry.min.js"></script>
    <script src="js/jquery.infinitescroll.min.js"></script>
<script src="js/imageUpload.js"></script>
<script src="js/videoUpload.js"></script>

<!--Script for Searching posts-->
<script type="text/javascript">
  $("#search").keyup(function(){
    var search = $("#search").val();
    if(search != ''){
      $('#results').css({'display': 'block'});
      $('#results').html("<img src='img/loader.gif' style='margin: 20px auto 20px 45%''> <br> <div style='text-align:center;'>Retriving topics...</div>");
      $('#results').load('pages/search.php', {"search": search});
    }else{
      $('#results').css({'display': 'none'}); 
      $('#results').html("");
    }
  });

  var videos = document.getElementsByTagName('video');
  var count = videos.length;

  for (var i = 0; i < count; i++) {
    //videos[i].addEventListener('mouseenter', video_controls_1,false);
   // videos[i].addEventListener('mouseout', video_controls_2,false);
    
    videos[i].pause();
  };
/*
  function video_controls_1(){
    this.controls = true;
  }
  function video_controls_2(){
    this.controls = false;
  }
  */

  function show_btn(e){
    var btn = document.getElementById('btn_'+e);

    btn.style.visibility =  "visible";

    btn.style.cursor =  "pointer";

    btn.addEventListener("click", video_play, true);
  }

  function hide_btn(e){
    var btn = document.getElementById('btn_'+e);

    btn.style.visibility =  "hidden";
  }

  
  function video_play(e){
    setTimeout(function(){
      var skip_btn = document.getElementById('skip_'+e);

      skip_btn.style.display = "inline";
    },10000);

    var video = document.getElementById('vid_'+e);

    //get original source and store in localstorage
    var source = $('#vid_'+e+' > source').attr('src');
    localStorage.setItem('source', source);

    //get id and store in localstorage
    localStorage.setItem('id', e);    

    if(video.paused){
      
      $("#btn_"+e).attr('class','fa fa-pause fa-3x video_button');
      //get the current time
      var current_times = video.currentTime;
      if(current_times > 0){
          video.play();
      }else{
          $('#vid_'+e+' > source').attr('src', 'video.mp4');   
          $('#vid_'+e).get(0).load();
          $('#vid_'+e).get(0).play();  
      }

    }else{ 
      $('#vid_'+e).get(0).pause();
      $("#btn_"+e).attr('class','fa fa-play fa-3x video_button');
    }
  }

    var id = localStorage.getItem('id');
    var source = localStorage.getItem('source');

    var video = document.getElementById('vid_'+id);

    setInterval(function(){
      //get duration of video
      var duration = video.duration;

      duration = Math.ceil(duration,2);

      //get the current time
      var current_time = video.currentTime;

      current_time = Math.ceil(current_time,2);

      if(current_time == duration){

        $('#vid_'+id+' > source').attr('src', source);   
        $('#vid_'+id).get(0).load();
        $('#vid_'+id).get(0).play(); 
      }},100);

  function skip_ad(e){
      $('#vid_'+e).get(0).pause();
      $('#vid_'+e+' > source').attr('src', localStorage.getItem('source'));   
      $('#vid_'+e).get(0).load();
      $('#vid_'+e).get(0).play(); 

      var skip_btn = document.getElementById('skip_'+e);

      skip_btn.style.display = "none";
  }
</script>

   <?php
}else{
?>
<!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/vegas/vegas.min.js"></script>

    <!-- jQuery Include -->
    <script src="libraries/jquery.easing.min.js"></script><!-- Easing Animation Effect -->    <script src="libraries/jquery.animateNumber.min.js"></script> <!-- Used for Animated Numbers -->
    <script src="libraries/jquery.appear.js"></script> <!-- It Loads jQuery when element is appears -->
    <script src="libraries/jquery.knob.js"></script> <!-- Used for Loading Circle -->
    <script src="libraries/wow.min.js"></script> <!-- Use For Animation -->
    <script src="libraries/flexslider/jquery.flexslider-min.js"></script> <!-- flexslider   -->
    <script src="libraries/owl-carousel/owl.carousel.min.js"></script> <!-- Core Owl Carousel CSS File  *   v1.3.3 -->
    <script src="libraries/expanding-search/modernizr.custom.js"></script> <!-- Core Owl Carousel CSS File  *   v1.3.3 -->
    <script src="libraries/expanding-search/classie.js"></script> 
    <script src="libraries/expanding-search/uisearch.js"></script>
    <script>
        new UISearch( document.getElementById( 'sb-search' ) );
    </script>
    <script type="text/javascript" src="libraries/jssor.js"></script>
    <script type="text/javascript" src="libraries/jssor.slider.js"></script>
    <script type="text/javascript" src="libraries/jquery.marquee.js"></script>
    <!-- Customized Scripts -->
    <script src="js/functions.js"></script>

    <script>
$('#body').vegas({
    slides: [
        { src: 'img/slide/1.jpg' },
        { src: 'img/slide/2.jpg' },
        { src: 'img/slide/3.jpg' }
    ],
    delay: 7000,
    overlay: "js/vegas/overlays/06.png"
});
    </script>
<?php
}
    ?>
</body>

</html>
