<?php

include('connect.php');

$type = secureTxt($_REQUEST['type']);
$following = secureTxt($_REQUEST['following']);
$username = secureTxt($_SESSION['logged_user']);

if ($type == 'follow') {
	$q = $conn->prepare("INSERT INTO follower (username, following, date, time) VALUES (:user, :following, :d, :t)");
	$q->bindParam(':user', $username);
	$q->bindParam(':following', $following);
	$q->bindParam(':d', $d);
	$q->bindParam(':t', $t);

	if ($q->execute()) {
		?>
<script>
$('#followBtn').css('display', 'none');
$('#unfollowBtn').css('display', 'inline');
$('#profile-loader').css('display', 'block');
$('#profile-review').load('include/profile_review.php');

</script>
<?php
	}
	//end of following user
}elseif ($type == 'unfollow') {
	# code...
	$q = $conn->prepare("DELETE FROM follower WHERE username = :user AND following = :following");
	$q->bindParam(':user', $username);
	$q->bindParam(':following', $following);

	if ($q->execute()) {
		?>
<script>
$('#unfollowBtn').css('display', 'none');
$('#followBtn').css('display', 'inline');
$('#profile-loader').css('display', 'block');
$('#profile-review').load('include/profile_review.php');
</script>
<?php
	}

	}
	//end of following user

?>
<script>
$('#follow-loader').css('display', 'none');
</script>